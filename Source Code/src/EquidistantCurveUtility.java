

import java.util.*;

import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.DecompositionSolver;
import org.apache.commons.math3.linear.LUDecomposition;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;

import uk.co.geolib.geolib.C2DLine;
import uk.co.geolib.geolib.C2DPoint;

public class EquidistantCurveUtility {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			VectorList point1 = new VectorList(2);
			point1.setElement(1, 0.6375);
			point1.setElement(2, -0.1875);
			VectorList point2 = new VectorList(2);
			point2.setElement(1, 0.5425);
			point2.setElement(2, -0.385);
			List<VectorList> points = findPoints(point1, point2);
//			System.out.println(findDistance(0, 0, points.get(0).getElement(1),points.get(0).getElement(2)));
//			System.out.println(findDistance(0, 0, points.get(1).getElement(1),points.get(1).getElement(2)));
		} catch (InvalidIndexValueException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	@SuppressWarnings("finally")
	/*
	 * This method returns (3 points) the points of intersection of the hyperbolic line and bounding circle and its corresponding center of hyperbolic line
	 */
	public static List<VectorList> findPoints(VectorList p1, VectorList p2){
		
		RealVector centerHyperbolicLine;
		ArrayList<VectorList> points = new ArrayList<>();
		try {
			centerHyperbolicLine = findCenterHyperbolicLine(p1.getElement(1), p1.getElement(2), p2.getElement(1), p2.getElement(2));
			double r2 = findDistance(centerHyperbolicLine.getEntry(0), centerHyperbolicLine.getEntry(1), p1.getElement(1), p1.getElement(2));
//			System.out.println("Radius r2 is: " + r2);
			RealVector centerBoundingCircle = new ArrayRealVector(new double[] { 0, 0}, false);
			RealVector[] pointsOfIntersection = findIntersectionTwoCircles(centerBoundingCircle, 1, centerHyperbolicLine, r2);
//			double check1 = findDistance(0, 0, 0.6378737723708603, -0.7701409290002498);
//			System.out.println("Should be 1: " + check1);
//			double check2 = findDistance(0, 0, 0.9997657871595156, -0.0216418766587827);
//			System.out.println("Should be 1: " + check2);
			
			VectorList point1 = new VectorList(2);
			point1.setElement(1, pointsOfIntersection[0].getEntry(0));
			point1.setElement(2, pointsOfIntersection[0].getEntry(1));
			VectorList point2 = new VectorList(2);
			point2.setElement(1, pointsOfIntersection[1].getEntry(0));
			point2.setElement(2, pointsOfIntersection[1].getEntry(1));
			points.add(point1);
			points.add(point2);
			VectorList centerHL = new VectorList(2);
			centerHL.setElement(1, centerHyperbolicLine.getEntry(0));
			centerHL.setElement(2, centerHyperbolicLine.getEntry(1));
			points.add(centerHL);
//			System.out.println("Distance 1 jhfkbjg: "+ findDistance(0, 0, point1.getElement(1), point1.getElement(2)));
//			System.out.println("Distance 2 jhfkbjg: "+ findDistance(0, 0, point2.getElement(1), point2.getElement(2)));
			
		} catch (InvalidIndexValueException e) {
			e.printStackTrace();
		}
		finally{
			//return null;
			return points;
		}
	}
	
	public static RealVector[] findIntersectionTwoCircles(RealVector c1, double r1, RealVector c2, double r2){
		
		//Source: https://fypandroid.wordpress.com/2011/07/03/how-to-calculate-the-intersection-of-two-circles-java/
		double x1 = c1.getEntry(0);
		double y1 = c1.getEntry(1);
		double x2 = c2.getEntry(0);
		double y2 = c2.getEntry(1);
		
		double d = Math.sqrt(Math.pow((x2 - x1),2) + Math.pow((y2 - y1), 2));
		if(d > r1 + r2){
//			System.out.println("Two circles won't intersect each other.");
			return null;
		}
		double d1 = (Math.pow(r1, 2) - Math.pow(r2, 2) + Math.pow(d, 2))/(2 * d);
		double h = Math.sqrt(Math.pow(r1, 2) - Math.pow(d1, 2));
		double x3 = x1 + (d1 * (x2 - x1))/d;
		double y3 = y1 + (d1 * (y2 - y1))/d;
		double x4 = x3 + (h * (y2 - y1))/d;
		double y4 = y3 - (h * (x2 - x1))/d;
		double x5 = x3 - (h * (y2 - y1))/d;
		double y5 = y3 + (h * (x2 - x1))/d;
		
		if(d == r1 + r2){
//			System.out.print("The circles just touch each other at the point: ");
//			System.out.println("("+x4+", "+y4+ ")");
			RealVector[] points = new RealVector[1];
			points[0] = new ArrayRealVector(new double[] { x4, y4 }, false);
			return points;
		}
//		System.out.print("Points of intersection of the two circles are: ");
//		System.out.println("("+x4+", "+y4+ ") & ("+ x5+", "+ y5+")");
		
		RealVector[] points = new RealVector[2];
		points[0] = new ArrayRealVector(new double[] { x4, y4 }, false);
		points[1] = new ArrayRealVector(new double[] { x5, y5 }, false);
		return points;
		
	}
	
	public static RealVector findCenterHyperbolicLine(double x1, double y1, double x2, double y2){
		
		double c1 = (Math.pow(x1, 2) + Math.pow(y1, 2) + 1)/2.0;
		double c2 = (Math.pow(x2, 2) + Math.pow(y2, 2) + 1)/2.0;
		
		RealMatrix coefficients =
			    new Array2DRowRealMatrix(new double[][] { { x1, y1 }, { x2, y2 }},
			                       false);
		DecompositionSolver solver = new LUDecomposition(coefficients).getSolver();
			
		RealVector constants = new ArrayRealVector(new double[] { c1, c2 }, false);
		RealVector solution = solver.solve(constants);
		
//		System.out.println(solution);
		return solution;
	}
	
	public static double findDistance(double x1, double y1, double x2, double y2){
		return Math.sqrt(Math.pow((x2 - x1),2) + Math.pow((y2 - y1), 2));
	}
	
	public static VectorList findMidPoint(VectorList p1, VectorList p2) throws InvalidIndexValueException{
		VectorList mid = new VectorList(2);
		mid.setElement(1, (p1.getElement(1) + p2.getElement(1))/2);
		mid.setElement(2, (p1.getElement(2) + p2.getElement(2))/2);
		return mid;
	}
	
	@SuppressWarnings("finally")
	public static double findAngle(VectorList p1, VectorList p2){
		double slope = 0;
		double deltay = 0, deltax = 0;
		try {
			if((p2.getElement(1) - p1.getElement(1)) == 0)
				throw new IllegalArgumentException();
			deltay = p2.getElement(2) - p1.getElement(2);
			deltax = p2.getElement(1) - p1.getElement(1);
			slope = deltay/deltax; 
		    	
		    //slope =	Math.atan2((p2.getElement(2) - p1.getElement(2)), (p2.getElement(1) - p1.getElement(1)) );
		} catch (InvalidIndexValueException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e){
			System.out.println("Division by zero");
		}
		finally{
			slope = Math.toDegrees(Math.atan2(deltay, deltax));
			//if (slope < 0)
				//slope += 360;
			return slope;
		}
		
	}

}
