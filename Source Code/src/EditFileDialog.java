 /*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author 500380
 */
import java.awt.BorderLayout;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.Vector;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingUtilities;
import static javax.swing.WindowConstants.DISPOSE_ON_CLOSE;

class EditFileDialog extends JDialog {
    
    int p;
    int q;
    int sidesInCentralRegion;
    int numberOfColors;
    int reflectionType;

  Vector<Integer> reflectionColors;
  Vector<Integer> rotateColors;
  Vector<Vector<Integer> > transformationData;

  // Private widgets
  JPanel centralGrid;
  JPanel rotateColorsGrid;
  JPanel reflectColorsGrid;
  JPanel transformationDataGrid;

  JSpinner pSelecter;
  JSpinner qSelecter;
  JSpinner sidesInCentralRegionSelecter;
  JSpinner numberOfColorsSelecter;
  JComboBox reflectionTypeSelecter;
  
  //Dynamically allocated in showEvent method
  Vector<JSpinner> rotateColorWidgets;
  Vector<JSpinner> reflectColorWidgets;
  Vector<Vector<JComboBox > > transformationDataWidgets;
  


    public EditFileDialog() {
        super();
        numberOfColors = sidesInCentralRegion = p = q = 1;
        reflectionType = 1;
        initUI();
        showEvent();
    }

    public final void initUI() {
        
        setLayout(new BoxLayout(getContentPane(), BoxLayout.PAGE_AXIS));
        
        transformationData = new Vector<Vector<Integer> >();//TODO check
        reflectionColors  = new Vector<Integer> ();
        rotateColors  = new Vector<Integer> ();
                
        Integer layerValue = new Integer(1);
        Integer layerMin = new Integer(1);
        Integer layerMax = new Integer(99);//TODO No maximum value set
        Integer layerStep = new Integer(1);     
        SpinnerNumberModel model1 = new SpinnerNumberModel(layerValue, layerMin, layerMax, layerStep);
        
        Integer colorMax = new Integer(10);
        SpinnerNumberModel model2 = new SpinnerNumberModel(layerValue, layerMin, colorMax, layerStep);    
        SpinnerNumberModel model3 = new SpinnerNumberModel(layerValue, layerMin, layerMax, layerStep);
        SpinnerNumberModel model4 = new SpinnerNumberModel(layerValue, layerMin, layerMax, layerStep);
        
        centralGrid = new JPanel();
        centralGrid.setLayout(new BorderLayout());
        
        JPanel northpanel = new JPanel(new FlowLayout ());
        
        JPanel pPanel = new JPanel();
        pPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("P")); 
        pSelecter = new JSpinner();
        pSelecter.setModel(model1);
        pSelecter.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
               changeP();
            }
        });
        pPanel.add(pSelecter);
        //pPanel.setAlignmentX(0.5f);
        northpanel.add(pPanel);
        
        JPanel qPanel = new JPanel();
        qPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Q"));
        qSelecter = new JSpinner(); 
        qSelecter.setModel(model3);
        qPanel.add(qSelecter);
        //qPanel.setAlignmentX(0.10f);
        northpanel.add(qPanel);

        
        JPanel sidesPanel = new JPanel();
        sidesPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Sides in Central Region"));
        sidesInCentralRegionSelecter = new JSpinner(); 
        sidesInCentralRegionSelecter.setModel(model4);
        sidesPanel.add(sidesInCentralRegionSelecter);
        //sidesPanel.setAlignmentY(0.15f);
        northpanel.add(sidesPanel);
        
        
        JPanel westpanel = new JPanel();     
        JPanel reflectionTypePanel = new JPanel();
        reflectionTypePanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Reflection Type"));
        String[] reflectionTypes = { "Reflected Bisector", "Reflected Hypotenuse" };
        reflectionTypeSelecter = new JComboBox(reflectionTypes); 
        reflectionTypePanel.add(reflectionTypeSelecter);
        //reflectionTypePanel.setAlignmentX(0.20f);
        westpanel.add(reflectionTypePanel);
        
                  
        
        JPanel centerpanel = new JPanel();
        JPanel colorsPanel = new JPanel();
        colorsPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Number of Colors"));
        numberOfColorsSelecter = new JSpinner(); 
        numberOfColorsSelecter.setModel(model2);
        numberOfColorsSelecter.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
               changeRotateAndReflectColorWidgets();
            }
        });
        colorsPanel.add(numberOfColorsSelecter);
        //colorsPanel.setAlignmentX(0.20f);
        centerpanel.add(colorsPanel);
        
        
        
        JPanel eastPanel = new JPanel();
        rotateColorsGrid = new JPanel();
        rotateColorsGrid.setBorder(javax.swing.BorderFactory.createTitledBorder("Rotation Colors"));
        rotateColorWidgets = new Vector<JSpinner>();
        JSpinner rotatespinner = new JSpinner(); //TODo Check
        rotatespinner.setEnabled(false);//TODo Check
        rotatespinner.setValue(new Integer(1));
        rotateColorsGrid.add(rotatespinner);
        rotateColorWidgets.add(rotatespinner);//TODo Check
        eastPanel.add(rotateColorsGrid);
        
        reflectColorsGrid = new JPanel();
        reflectColorsGrid.setBorder(javax.swing.BorderFactory.createTitledBorder("Reflection Colors"));
        reflectColorWidgets = new Vector<JSpinner>();
        JSpinner reflectspinner = new JSpinner(); //TODo Check
        reflectspinner.setEnabled(false);//TODo Check
        reflectspinner.setValue(new Integer(1));
        reflectColorsGrid.add(reflectspinner);
        reflectColorWidgets.add(reflectspinner);//TODo Check        
        eastPanel.add(reflectColorsGrid);


        
        JPanel southPanel = new JPanel(new GridLayout(2,0));
        transformationDataGrid = new JPanel();
        transformationDataGrid.setBorder(javax.swing.BorderFactory.createTitledBorder("Transformation Data"));
        // Elements added in showEvent function
        transformationDataWidgets = new  Vector<Vector<JComboBox > >(); //TODO Check
        //transformationDataGrid.setAlignmentX(0.20f);
        southPanel.add(transformationDataGrid);
        
        
        
        
        JPanel southPanelpart2 = new JPanel(new FlowLayout ());  
        JButton okButton = new javax.swing.JButton();
        okButton.setText("Ok");
        okButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                accept();
            }
        });
        okButton.setAlignmentX(0.5f);
        southPanelpart2.add(okButton);
        
        JButton cancelButton = new javax.swing.JButton();
        cancelButton.setText("Cancel");
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                reject();
            }
        });
        cancelButton.setAlignmentX(0.5f);
        southPanelpart2.add(cancelButton);       
        southPanel.add(southPanelpart2);
        
        
        centralGrid.add(northpanel,BorderLayout.NORTH);
        centralGrid.add(westpanel,BorderLayout.WEST);
        centralGrid.add(centerpanel,BorderLayout.CENTER);
        centralGrid.add(eastPanel,BorderLayout.EAST);
        centralGrid.add(southPanel,BorderLayout.SOUTH);
        
        add(centralGrid);


        setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setLocationRelativeTo(null);
        this.setResizable(true);
        setSize(500, 400);
        //validate(); //TODO change the size of the dialog dynamically
        //repaint();
        //this.pack();
        
    }
    ////////////////////////////////////////////////////////////////////////////////
    // dialogInit
    ////////////////////////////////////////////////////////////////////////////////
    protected void showEvent() //TODO pass param
    {
      pSelecter.setValue(p);
      qSelecter.setValue(q);
      sidesInCentralRegionSelecter.setValue(sidesInCentralRegion);
      numberOfColorsSelecter.setValue(numberOfColors);
      changeRotateAndReflectColorWidgets();
      //showEvent(); //TODO Chcek this line
    } // showEvent
    
    

    
    ////////////////////////////////////////////////////////////////////////////////
    // addTransformationDataWidgets
    ////////////////////////////////////////////////////////////////////////////////
    /*!
    * \brief a utility function to add the dynamically allocated QWidgets for the
    * transformation data
    */
    
    
    
    void  addTransformationDataWidgets()
    {
        GridLayout experimentLayout = new GridLayout(p,1);
        transformationDataGrid.setLayout(experimentLayout);
        for( int i = 0; i < p; ++i)
        {
          Vector<JComboBox > temp = new Vector<JComboBox >();
          // Add orientation          
          Vector<String> comboBoxItems = new Vector<String>();
          for(int j = -p; j < 0; ++j)
          {  
            int number = -j; 
            comboBoxItems.add("Reflect for edge " + number);
          }
          
          for( int j = 1; j <= p; ++j)
          {
            comboBoxItems.add("Rotate for edge "+ j);
          }
          final DefaultComboBoxModel model = new DefaultComboBoxModel(comboBoxItems);
          JComboBox orientation = new JComboBox(model); 
          
          if(i < transformationData.size() && 0 < transformationData.elementAt(i).size())
          {
            int value = transformationData.elementAt(i).elementAt(0);
            if(value < 0)
            {
              orientation.setSelectedIndex(value + p);
            }
            else
            {
              orientation.setSelectedIndex(value + p - 1);
            }           
          }
          transformationDataGrid.add(orientation);//TODO SEt up the lay  out for each i 
          temp.add(orientation);
          
          for( int j = 1; j <= numberOfColors; ++j)
          {
            Vector items = new Vector();
            for( int k = 1; k <= numberOfColors; ++k)
            {
              items.add(k);
            }
            final DefaultComboBoxModel model1 = new DefaultComboBoxModel(items);
            JComboBox color = new JComboBox(model1);
            if(i < transformationData.size() && j < transformationData.elementAt(i).size())
            {
              color.setSelectedIndex(transformationData.elementAt(i).elementAt(j) - 1);
            }
            
            temp.add(color);
            
            transformationDataGrid.add(color); //TODO SEt up the lay  out for each i 
          }
          
          //System.out.println("the temp size "+temp.size());
          transformationDataWidgets.add(i, temp);
        }
        
        this.setSize(500+numberOfColors*100, 400+p*50);
      this.repaint();

    } // addTransformationDataWidgets
    
    ////////////////////////////////////////////////////////////////////////////////
    // changeRotateAndReflectColorWidgets
    ////////////////////////////////////////////////////////////////////////////////
    /*!
    * \brief a utility function to change the color permutations for rotate and
    * reflect colors
    */
    void changeRotateAndReflectColorWidgets(){
        numberOfColors = ((Integer)numberOfColorsSelecter.getValue()).intValue();     
        removeRotateAndReflectColorWidgets();
        addRotateAndReflectColorWidgets();
        removeTransformationDataWidgets();
        addTransformationDataWidgets();

    }
    ////////////////////////////////////////////////////////////////////////////////
    // addRotateAndReflectColorWidgets
    ////////////////////////////////////////////////////////////////////////////////
    /*!
    * \brief a utility function to add the dynamically allocated QWidgets for
    * color permutations for rotate and reflect colors
    */
    void addRotateAndReflectColorWidgets()
    {
      for( int i = 0; i < numberOfColors; ++i)
      {
        
        Integer layerValue = new Integer(1);
        Integer layerMin = new Integer(1);
        Integer layerMax = new Integer(numberOfColors);//TODO No maximum value set
        Integer layerStep = new Integer(1);     
        SpinnerNumberModel model1 = new SpinnerNumberModel(layerValue, layerMin, layerMax, layerStep);
        
        JSpinner rotateTemp = new JSpinner();
        rotateTemp.setModel(model1);
        if(i < rotateColors.size())
        {
          rotateTemp.setValue(rotateColors.elementAt(i));
        }
        else
        {
          rotateTemp.setValue(i+1); //TODO does the int needs to be converted to INteger???
        }
        rotateColorWidgets.add(rotateTemp);
        rotateColorsGrid.add(rotateTemp);//TODO Check Layout
        JSpinner reflectTemp = new JSpinner();
        reflectTemp.setModel(model1);
        if(i < reflectionColors.size())
        {
          reflectTemp.setValue(reflectionColors.elementAt(i));
        }
        else
        {
          reflectTemp.setValue(i+1);//TODO does the int needs to be converted to INteger???
        }
        reflectColorWidgets.add(reflectTemp);
        reflectColorsGrid.add(reflectTemp);//TODO Check Layout
      }
      this.setSize(500+numberOfColors*40, 400+p*60);
      this.repaint();
    } // addRotateAndReflectColorWidgets



    ////////////////////////////////////////////////////////////////////////////////
    // accept
    ////////////////////////////////////////////////////////////////////////////////
    /*!
    * \brief Overrides the standard accept slot
    */
    void accept(){
        boolean goodToGo = true;
        Vector<String> errorMessages = new Vector<String>();
        // Save Values
        numberOfColors = ((Integer)numberOfColorsSelecter.getValue()).intValue();
        p = ((Integer)pSelecter.getValue()).intValue();
        q = ((Integer)qSelecter.getValue()).intValue();
        reflectionType = reflectionTypeSelecter.getSelectedIndex()+1;
        sidesInCentralRegion = ((Integer)sidesInCentralRegionSelecter.getValue()).intValue();
        
        //System.out.println(p +" "+ q +" "+ reflectionType +" "+ sidesInCentralRegion+ " "+numberOfColors+"\n");
        rotateColors.clear();
        for( int i = 0; i < rotateColorWidgets.size(); ++i)
        {
          JSpinner temp = rotateColorWidgets.elementAt(i);
          rotateColors.add(((Integer)temp.getValue()).intValue());
        }
        reflectionColors.clear();
        for( int i = 0; i < reflectColorWidgets.size(); ++i)
        {
          JSpinner temp = reflectColorWidgets.elementAt(i);
          reflectionColors.add(((Integer)temp.getValue()).intValue());
        }

        for( int i = 0; i < transformationData.size(); ++i)
        {
          transformationData.elementAt(i).clear();
        }
        transformationData.clear();

        for( int i = 0; i < transformationDataWidgets.size(); ++i)
        {
          Vector<Integer> temp = new Vector<Integer>();
          // Get the orientation
          
          JComboBox combo = transformationDataWidgets.elementAt(i).elementAt(0);
          int index = combo.getSelectedIndex();
          if(index < p)
          {
            temp.add(index - p);
          }
          else
          {
            temp.add(index - p + 1);
          }
          for( int j = 0; j < numberOfColors; ++j)
          {
            //System.out.println("size "+transformationDataWidgets.elementAt(i).size()+" index: "+ j+1 );
            combo = transformationDataWidgets.elementAt(i).elementAt(j+1);//TODo changes j+1 to j 
            temp.add(combo.getSelectedIndex()+1);
          }
          
          transformationData.add(temp);
        }

        // Perform Checks
        if(p % sidesInCentralRegion != 0)
        {
          goodToGo = false;
          errorMessages.add("p must be divisible by the number of " +
                                  "sides in the central region");
        }
        // Clean up dynamically allocated widgets
        clean();
        // Accept only if cheks passed
        if(goodToGo)
        {
          //accept(); //TODO check this line again //Super.accept
            this.dispose();
        }
        else
        {
          // Display errors //TODO Write the else part

          for( int i = 0; i < errorMessages.size(); ++i)
          {
            JOptionPane.showMessageDialog(null,errorMessages.elementAt(i) );

          }
     }

    }
    
    ////////////////////////////////////////////////////////////////////////////////
    // changeP
    ////////////////////////////////////////////////////////////////////////////////
    /*!
    * \brief a utility function to handle changes needed when p changes
    */
    private  void changeP(){
        p = ((Integer)pSelecter.getValue()).intValue();
        removeTransformationDataWidgets();
        addTransformationDataWidgets();
    } 
    
    ////////////////////////////////////////////////////////////////////////////////
    // removeRotateAndReflectColorWidgets
    ////////////////////////////////////////////////////////////////////////////////
    /*!
    * \brief a utility function to remove the dynamically allocated QWidgets for
    * color permutations for rotate and reflect colors
    */
    void removeRotateAndReflectColorWidgets()
    {
        /*int size = rotateColorWidgets.size();
        for( int i = 0; i < size; ++i)
        {
          JSpinner temp = rotateColorWidgets.lastElement();
          rotateColorWidgets.removeElementAt(rotateColorWidgets.size()-1);
          temp = reflectColorWidgets.lastElement();
          reflectColorWidgets.removeElementAt(reflectColorWidgets.size()-1);
        }*/
        rotateColorsGrid.removeAll();
        reflectColorsGrid.removeAll();
        reflectColorWidgets.clear();
        rotateColorWidgets.clear();
        

    } // removeRotateAndReflectColorWidgets
    
    ////////////////////////////////////////////////////////////////////////////////
    // removeTransformationDataWidgets
    ////////////////////////////////////////////////////////////////////////////////
    /*!
    * \brief a utility function to remove the dynamically allocated QWidgets for
    * the tranformstion data
    */
    void removeTransformationDataWidgets()
    {
        
        /*int size = transformationDataWidgets.size();
        for( int i = 0; i < size; ++i)
        {
           int size2 = transformationDataWidgets.lastElement().size(); //TODo cehck again
          for( int j = 0; j < size2; ++j)
          {
            JComboBox temp = transformationDataWidgets.lastElement().lastElement();
            transformationDataWidgets.lastElement().removeElementAt(transformationDataWidgets.lastElement().size()-1);
          }
          transformationDataWidgets.removeElementAt(transformationDataWidgets.size()-1);
        }*/
        transformationDataGrid.removeAll();
        transformationDataWidgets.clear();
        

    } // removeTransformationDataWidgets
 
    ////////////////////////////////////////////////////////////////////////////////
    // getNumberOfColors
    ////////////////////////////////////////////////////////////////////////////////
    /*!
   * \brief Gets the number of colors for the new/changed motif
   *
   * \return The number of colors for the motif
   */
   int getNumberOfColors() 
   {
     return numberOfColors;
   } // getNumberOfColors

    ////////////////////////////////////////////////////////////////////////////////
    // getP
    ////////////////////////////////////////////////////////////////////////////////
   /*!
   * \brief Gets p for the new/changed motif
   *
   * \return p for the motif
   */
    int getP() 
    {
      return p;
    } // getP
    
    ////////////////////////////////////////////////////////////////////////////////
    // getQ
    ////////////////////////////////////////////////////////////////////////////////
    /*!
    * \brief Gets q for the new/changed motif
    *
    * \return q for the motif
    */
    int getQ() 
    {
      return q;
    } // getQ
    
    ////////////////////////////////////////////////////////////////////////////////
    // getReflectionColors
    ////////////////////////////////////////////////////////////////////////////////
    /*!
    * \brief Gets the reflection colors for the new/changed motif
    *
    * \return A vector of integers representing the color numbers
    */
    Vector<Integer> getReflectionColors() 
    {
      return reflectionColors;
    } // getReflectionColors
    
    ////////////////////////////////////////////////////////////////////////////////
    // getReflectionType
    ////////////////////////////////////////////////////////////////////////////////
    /*!
    * \brief Gets the reflection type for the new/changed motif
    *
    * \return The reflection type for the motif
    */
    int getReflectionType() 
    {
      return reflectionType;
    } // getReflectionType

    ////////////////////////////////////////////////////////////////////////////////
    // getRotationColors
    ////////////////////////////////////////////////////////////////////////////////
    /*!
    * \brief Gets the rotation colors for the new/changed motif
    *
    * \return A vector of integers representing the color numbers
    */
    Vector<Integer> getRotationColors() 
    {
      return rotateColors;
    } // getRotationColors

    ////////////////////////////////////////////////////////////////////////////////
    // getSidesInCentralRegion
    ////////////////////////////////////////////////////////////////////////////////
    /*!
    * \brief Gets the number of sides in the central region for the new/changed
    * motif
    *
    * \return The number of sides in the central region for the motif
    */
    int getSidesInCentralRegion() 
    {
      return sidesInCentralRegion;
    } // getSidesInCentralRegion

    ////////////////////////////////////////////////////////////////////////////////
    // getTransformationInformation
    ////////////////////////////////////////////////////////////////////////////////
    /*!
    * \brief Gets the Transformation information for the new/changed motif
    *
    * \return An vector of integer vectors where the first element of each inner
    * vector is the information for orientation and the remaining elements are
    * the color permutation information
    */
    Vector<Vector<Integer> > getTransformationInformation() 
    {
      return transformationData;
    } // getTransformationInformation

    ////////////////////////////////////////////////////////////////////////////////
    // setNumberOfColors
    ////////////////////////////////////////////////////////////////////////////////
    /*!
    * \brief Sets the number of colors for the new/changed motif
    *
    * This function only updates if 0 < n < 11
    * \param n The number of colors to set
    */
    void setNumberOfColors(int n)
    {
      if(n > 0 && n < 11)
      {
        numberOfColors = n;
      }
    } // setNumberOfColors

    ////////////////////////////////////////////////////////////////////////////////
    // setP
    ////////////////////////////////////////////////////////////////////////////////
    /*!
    * \brief Sets p for the new/changed motif
    *
    * This function only updates if 0 < p
    * \param p The new p
    */
    void setP(int p)
    {
      if(p > 0)
      {
        this.p = p;
      }
    } // setP

    ////////////////////////////////////////////////////////////////////////////////
    // setQ
    ////////////////////////////////////////////////////////////////////////////////
    /*!
    * \brief Sets q for the new/changed motif
    *
    * This function only updates if 0 < q
    * \param q The new q
    */
    void setQ(int q)
    {
      if(q > 0)
      {
        this.q = q;
      }
    } // setQ

    ////////////////////////////////////////////////////////////////////////////////
    // setReflectionColors
    ////////////////////////////////////////////////////////////////////////////////
    /*!
    * \brief Sets the reflection colors for the new/changed motif
    *
    * \param r A vector of integers representing the color numbers
    */
    void setReflectionColors(Vector<Integer> r)
    {
      reflectionColors = r;
    } // setReflectionColors

    ////////////////////////////////////////////////////////////////////////////////
    // setReflectionType
    ////////////////////////////////////////////////////////////////////////////////
    /*!
    * \brief Sets the reflection type for the new/changed motif
    *
    * This function only updates if 0 < reflectionType < 3
    * \param reflectionType The reflection type
    */
    void setReflectionType(int reflectionType)
    {
      if(reflectionType > 0 && reflectionType < 3)
      {
        this.reflectionType = reflectionType;
      }
    } // setReflectionType

    ////////////////////////////////////////////////////////////////////////////////
    // setRotationColors
    ////////////////////////////////////////////////////////////////////////////////
    /*!
    * \brief Sets the rotation colors for the new/changed motif
    *
    * \param r A vector of integers representing the color numbers
    */
    void setRotationColors(Vector<Integer> r)
    {
      rotateColors = r;
    } // setRotationColors

    ////////////////////////////////////////////////////////////////////////////////
    // setSidesInCentralRegion
    ////////////////////////////////////////////////////////////////////////////////
    /*!
    * \brief Sets the number of sides in the central region for the new/changed
    * motif
    *
    * This function only updates if 0 < s
    * \param s The number of sides in the central region
    */
    void setSidesInCentralRegion(int s)
    {
      if((p % s)!=0)
      {
        sidesInCentralRegion = s;
      }
    } // setSidesInCentralRegion

    ////////////////////////////////////////////////////////////////////////////////
    // setTransformationInformation
    ////////////////////////////////////////////////////////////////////////////////
    /*!
    * \brief Sets the transformation information for the new/changed motif
    *
    * \param t The information
    */
    void setTransformationInformation(Vector<Vector<Integer> > t)
    {
      transformationData = t;
    } // setTransformationInformation
    
    ////////////////////////////////////////////////////////////////////////////////
    // close
    ////////////////////////////////////////////////////////////////////////////////
    /*!
    * \brief Overrides the standar close slot
    */
    void close()
    {
       clean();
       this.dispose();
    } // close

    ////////////////////////////////////////////////////////////////////////////////
    // reject
    ////////////////////////////////////////////////////////////////////////////////
    /*!
    * \brief Overrides the standard reject slot
    */
    void reject()
    {
        clean();
       this.dispose();
    } // reject
    
    ////////////////////////////////////////////////////////////////////////////////
    // clean
    ////////////////////////////////////////////////////////////////////////////////
    /*!
    * \brief a utility function to clean up dynamically allocated QWidgets
    */
    void clean()
    {
      // Remove dynamically allocated QWidgets
      removeRotateAndReflectColorWidgets();
      removeTransformationDataWidgets();
    } // clean
    
    
    


    
}
