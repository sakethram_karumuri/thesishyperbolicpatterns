/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author maneeshavejendla
 */
public class DimensionMismatchException extends Exception {
    public DimensionMismatchException(){
        super();
    }
     public DimensionMismatchException(String message){
        super(message); // TODO or super("Invalid Dimension for the Operation Specified");
    }
    
}
