
import java.awt.Color;
import java.awt.Shape;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author 500380
 */
public class GraphicsItem {
    Shape graphicsItem;
    Color color;
    boolean brush;
    GraphicsItem(){
        color = Color.BLACK;
        brush = false;
    }
    GraphicsItem(Shape graphicsItem){
        this.graphicsItem = graphicsItem;
        this.color  =  Color.BLACK;
        this.brush = false;
    }
    GraphicsItem(Shape graphicsItem, Color pcolor, boolean isBrush){
        this.graphicsItem = graphicsItem;
        this.color  =  pcolor;
        this.brush  =  isBrush;
    }
    
}
