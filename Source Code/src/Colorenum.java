
import java.awt.Color;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author maneeshavejendla
 * 
 * 
 * 
 */
  enum DefinedColor
  {
    COLOR_GREY,
    COLOR_BLACK,
    COLOR_WHITE,
    COLOR_RED,
    COLOR_GREEN,
    COLOR_BLUE,
    COLOR_CYAN,
    COLOR_MAGENTA,
    COLOR_YELLOW,
    COLOR_SALMON,
    COLOR_BROWN;
}// Color

public class Colorenum{
    
  
   DefinedColor colorDefined;
   Colorenum(){
       
   }
   
   Colorenum(DefinedColor colorDefined){
       this.colorDefined = colorDefined;
   }
   
   DefinedColor getColorEnum(){
       //System.out.println(colorDefined);
       return colorDefined;
   }
   
//TODO Check on returning the return statement of the following function
////////////////////////////////////////////////////////////////////////////////
// getColorFromInt
////////////////////////////////////////////////////////////////////////////////
static DefinedColor getColorFromInt(int color)
    throws InvalidIndexValueException
{
  switch(color)
  {
  case 0:
    return DefinedColor.COLOR_GREY; // #BEBEBE (190, 190, 190)
  case 1:
    return  DefinedColor.COLOR_BLACK; // #000000 (0, 0, 0)
  case 2:
    return  DefinedColor.COLOR_WHITE; // #FFFFFF (255, 102, 204)
  case 3:
    return  DefinedColor.COLOR_RED; // #FF0000 (255, 0, 0)
  case 4:
    return  DefinedColor.COLOR_GREEN; // #00FF00 (0, 255, 0)
  case 5:
    return  DefinedColor.COLOR_BLUE; // #0000FF (0, 0, 255)
  case 6:
    return  DefinedColor.COLOR_CYAN; // #00FFFF (0, 255, 255)
  case 7:
    return  DefinedColor.COLOR_MAGENTA; // #FF00FF (255, 0, 255)
  case 8:
    return  DefinedColor.COLOR_YELLOW; // #FFFF00 (255, 255, 0)
  case 9:
    return  DefinedColor.COLOR_SALMON; // #FA8072 (250, 128, 114)
  case 10:
    return  DefinedColor.COLOR_BROWN; // #A52A2A (165, 42, 42)
  default:
    throw new InvalidIndexValueException();
  }
} // getColorFromInt
//TODO Check on returning the return statement of the following function

////////////////////////////////////////////////////////////////////////////////
// getColorFromDefinedColor
////////////////////////////////////////////////////////////////////////////////
static Color getColorFromDefinedColor(DefinedColor color) throws InvalidIndexValueException
{
  switch(color)
  {
  case COLOR_GREY:
     return new Color(190, 190, 190);
  case  COLOR_BLACK:
    return new Color(0, 0, 0);
  case  COLOR_WHITE:
    return new Color(255, 255, 255);
  case  COLOR_RED:
    return new Color(255, 0, 0);
  case  COLOR_GREEN:
    return new Color(0, 255, 0);
  case  COLOR_BLUE:
    return new Color(0, 0, 255);
  case  COLOR_CYAN:
    return new Color(0, 255, 255);
  case  COLOR_MAGENTA:
    return new Color(255, 0, 255);
  case  COLOR_YELLOW:
    return new Color(255, 255, 0);
  case  COLOR_SALMON:
    return new Color(250, 128, 114);
  case  COLOR_BROWN:
    return new Color(165, 42, 42);
  default:
   throw new InvalidIndexValueException();
  }

} ////////////////////////////////////////////////////////////////////////////////
// getIntFromDefinedColor
////////////////////////////////////////////////////////////////////////////////
static int getIntFromDefinedColor(DefinedColor color)
{
   int returnValue = -1;

  switch(color)
  {
  case  COLOR_BLACK:
    returnValue = 1;
    break;
  case  COLOR_WHITE:
    returnValue= 2;
    break;
  case  COLOR_RED:
    returnValue= 3;
    break;
  case  COLOR_GREEN:
    returnValue= 4;
    break;
  case  COLOR_BLUE:
    returnValue= 5;
    break;
  case  COLOR_CYAN:
    returnValue= 6;
    break;
  case  COLOR_MAGENTA:
    returnValue= 7;
    break;
  case  COLOR_YELLOW:
    returnValue= 8;
    break;
  case  COLOR_SALMON:
    returnValue= 9;
    break;
  case  COLOR_BROWN:
    returnValue= 10;
    break;
  default:
    // Grey
    returnValue= 0;
    break;
  }

  return returnValue;
} // getIntFromDefinedColor

////////////////////////////////////////////////////////////////////////////////
// getQColorFromColor
////////////////////////////////////////////////////////////////////////////////
Color getQColorFromColor(DefinedColor color)
{
  Color returnValue;

  switch(color)
  {
  case  COLOR_BLACK:
    returnValue = new Color(0, 0, 0);
    break;
  case  COLOR_WHITE:
    returnValue= new Color(255, 255, 255);
    break;
  case  COLOR_RED:
    returnValue= new Color(255, 0, 0);
    break;
  case  COLOR_GREEN:
    returnValue= new Color(0, 255, 0);
    break;
  case  COLOR_BLUE:
    returnValue= new Color(0, 0, 255);
    break;
  case  COLOR_CYAN:
    returnValue= new Color(0, 255, 255);
    break;
  case  COLOR_MAGENTA:
    returnValue= new Color(255, 0, 255);
    break;
  case  COLOR_YELLOW:
    returnValue= new Color(255, 255, 0);
    break;
  case  COLOR_SALMON:
    returnValue= new Color(250, 128, 114);
    break;
  case  COLOR_BROWN:
    returnValue= new Color(165, 42, 42);
    break;
  default:
    // Grey
    returnValue= new Color(190, 190, 190);
    break;
  }

  return returnValue;
} // getColorFromDefinedColor


////////////////////////////////////////////////////////////////////////////////
// colorToString
////////////////////////////////////////////////////////////////////////////////
static String colorToQString(DefinedColor color)
{
  String colorString= null;
  switch(color)
  {
  case COLOR_BLACK:
    colorString =  "Black";
    break;
  case  COLOR_WHITE:
    colorString =  "White";
    break;
  case  COLOR_RED:
    colorString =  "Red";
    break;
  case  COLOR_GREEN:
    colorString = "Green";
    break;
  case  COLOR_BLUE:
    colorString =  "Blue";
    break;
  case  COLOR_CYAN:
    colorString = "Cyan";
    break;
  case  COLOR_MAGENTA:
    colorString = "Magenta";
    break;
  case  COLOR_YELLOW:
    colorString =  "Yellow";
    break;
  case  COLOR_SALMON:
    colorString = "Salmon";
    break;
  case  COLOR_BROWN:
    colorString = "Brown";
    break;
  default:
    colorString = "Grey";
    break;
      
      //return colorString;
  }
  return colorString;
} // colorToString
public static void main(String[] args) throws InvalidIndexValueException{
    System.out.println("Testing the colorEnum Class");
    Colorenum c = new Colorenum();
    String stringColor  = c.colorToQString(DefinedColor.COLOR_RED);
    String stringColor2  = c.colorToQString(DefinedColor.COLOR_BLACK);
    System.out.println("String converted colors are: ");
    System.out.println(stringColor);
    System.out.println(stringColor2);
    //TODO how to test these
    c.getColorFromInt(1);
    c.getColorFromInt(2);
    Color colr = c.getQColorFromColor(DefinedColor.COLOR_RED);
    System.out.println(colr.toString());
    
}
   
} 