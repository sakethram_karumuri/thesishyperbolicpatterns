/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author maneeshavejendla
 */

/*!
 * \class Matrix
 *
 * \brief A  class that represents a mathematical matrix.
 *
 * Matrix will need the multiplication, subtraction, and addition functions
 * defined, as well as a constructor that takes no arguments and the ability to
 * convert 0 and -1 into its type.
 */

 public class Matrix {
    private  int numberOfRows;             // number of rows
    private  int numberOfColumns;             // number of columns
    private  double[][] array;   // M-by-N array
    
    
  ////////////////////////////////////////////////////////////////////////////////
  // Matrix Construcor
  ////////////////////////////////////////////////////////////////////////////////
  /*!
   * \brief Matrix Constructor
   *
   * Creates a rows x columns mathematical matrix and initializes all elements
   * to initValue.
   * \param rows The number of rows
   * \param columns The number of columns
   * \param initValue the initial value 
   * \throws InvalidIndexValueException if rows < 1 or columns < 1
   */
    
  Matrix(int rows, int columns, double initValue)
      throws InvalidIndexValueException
  {
    if(rows > 0 && columns > 0)
    {
      numberOfRows = rows;
      numberOfColumns = columns;
      array = new  double[numberOfRows][numberOfColumns];
      int i, j;
      for(i = 0; i < numberOfRows; ++i)
      {
        for(j = 0; j < numberOfColumns; ++j)
        {
          array[i][j] = initValue;
        }
      }
    }
    else
    {
      throw new InvalidIndexValueException();
    }
  } // Constructor
    
  ////////////////////////////////////////////////////////////////////////////////
  // Matrix Construcor
  ////////////////////////////////////////////////////////////////////////////////
    
    
  Matrix(int rows, int columns) throws InvalidIndexValueException{
      this(rows,columns,0.0);
  }// Constructor
  
  
  ////////////////////////////////////////////////////////////////////////////////
  // Matrix Construcor
  ////////////////////////////////////////////////////////////////////////////////

  
   /*!
   * \brief Matrix Copy Constructor
   *
   * \param m Constant Matrix to be copied
   */
  Matrix(final Matrix m)
  {
    numberOfRows = m.numberOfRows;
    numberOfColumns = m.numberOfColumns;
    array = new double[numberOfRows][numberOfColumns];
    int i, j;
    for(i = 0; i < numberOfRows; ++i)
    {
      for(j = 0; j < numberOfColumns; ++j)
      {
        array[i][j] = m.array[i][j];
      }
    }
  } // Copy Constructor
  
  
  ////////////////////////////////////////////////////////////////////////////////
  // getNumberOfColumns
  ////////////////////////////////////////////////////////////////////////////////
  
  /*!
   * \brief Get the number of columns in the matrix.
   *
   * \return The number of columns in the matrix
   */
  int getNumberOfColumns() 
  {
    return numberOfColumns;
  } // getNumberOfColumns

  ////////////////////////////////////////////////////////////////////////////////
  // getNumberOfRows
  ////////////////////////////////////////////////////////////////////////////////
  
  /*!
   * \brief Get the number of rows in the matrix.
   *
   * \return The number of rows in the matrix
   */
  int getNumberOfRows() 
  {
    return numberOfRows;
  } // getNumberOfRows
  
  
  
  ////////////////////////////////////////////////////////////////////////////////
  // setElement
  ////////////////////////////////////////////////////////////////////////////////
  
   /*!
   * \brief Set element located in the selected row and column
   *
   * \param row Row of the element
   *    where 1 <= row <= getNumberOfRows()
   * \param column Column of the element
   *    where 1 <= column <= getNumberOfColumns()
   * \param value The value to assign to the element
   * \throws InvalidIndexValueException if row < 1 or row > getNumberOfRows()
   *    or column < 1 or column > numberOfColumns()
   */
  void setElement(int row, int column, double value)
      throws InvalidIndexValueException
  {
    if(row < 1 || row > getNumberOfRows() ||
       column < 1 || column > getNumberOfColumns())
    {
      throw new InvalidIndexValueException();
    }
    array[row - 1][column -1] = value;
  } // setElement
  
  
  
  ////////////////////////////////////////////////////////////////////////////////
  // getRow
  ////////////////////////////////////////////////////////////////////////////////
  
   /*!
   * \brief Get a copy of a row from the matrix
   *
   * \param row The index of the row to get
   *
   * \throws InvalidIndexValueException if row < 1 or row > getNumberOfRows()
   */
  VectorList getRow(int row) 
      throws InvalidIndexValueException
  {
    if(row < 1 || row > numberOfRows)
    {
      throw new InvalidIndexValueException();
    }
    VectorList returnValue = new VectorList(numberOfColumns);
    int i;
    for(i = 0; i < numberOfColumns; ++i)
    {
      returnValue.setElement(i + 1, array[row - 1][i]);
    }
    return returnValue;
  } // getRow
  
  
  
  ////////////////////////////////////////////////////////////////////////////////
  // getColumn
  ////////////////////////////////////////////////////////////////////////////////
  
   /*!
   * \brief Get a copy of a column from the matrix
   *
   * \param column The index of the column to get
   *
   * \throws InvalidIndexValueException if column < 1 or
   *    column > getNumberOfColumns()
   */
  VectorList getColumn(int column) 
      throws InvalidIndexValueException 
  {
    if(column < 1 || column > numberOfColumns)
    {
      throw new InvalidIndexValueException();
    }
    VectorList returnValue = new VectorList(numberOfRows);
    int i;
    for(i = 0; i < numberOfRows; ++i)
    {
      returnValue.setElement(i + 1, array[i][column - 1]);
    }
    return returnValue;
  } // getColumn

  
  
  
  
  ////////////////////////////////////////////////////////////////////////////////
  // equals
  ////////////////////////////////////////////////////////////////////////////////
  
   /*!
   * \brief = operator implementation
   *
   * \param rhs Constant matrix to assign to the current matrix
   *
   * \return A reference to the current matrix after being assigned to
   */
  Matrix equals(final Matrix rhs)
  {
    if(rhs != this)
    {
      array = null;
      numberOfRows = rhs.numberOfRows;
      numberOfColumns = rhs.numberOfColumns;
      array = new double[numberOfRows][numberOfColumns];

      int i, j;
      for(i = 0; i < numberOfRows; ++i)
      {
        for(j = 0; j < numberOfColumns; ++j)
        {
          array[i][j] = rhs.array[i][j];
        }
      }
    }
    return this;
  } // equals
  
  
  
  ////////////////////////////////////////////////////////////////////////////////
  // add
  ////////////////////////////////////////////////////////////////////////////////
  
  /*!
   * \brief + operator implementation
   *
   * \param rhs Constant matrix to add to the current matrix
   *
   * \return A constant matrix that is the sum of the two matrices
   *
   * \throws DimensionMismatchException if the number of rows in the current
   *    matrix does not match the number of rows in rhs or the number of columns
   *    in the current matrix does not match the number of columns in rhs
   */
  final Matrix add(final Matrix rhs) 
      throws DimensionMismatchException
  {
    Matrix returnValue = this;
    returnValue.plusEquals(rhs);
    return returnValue;
  } // add
  
  

  ////////////////////////////////////////////////////////////////////////////////
  // plusEquals
  ////////////////////////////////////////////////////////////////////////////////
  
  /*!
   * \brief += operator implementation
   *
   *
   * \param rhs Constant matrix to add and assign to the current matrix
   *
   * \return A reference to the current matrix after being added to
   *
   * \throws DimensionMismatchException if the number of rows in the current
   *    matrix does not match the number of rows in rhs or the number of columns
   *    in the current matrix does not match the number of columns in rhs
   */
  Matrix plusEquals(final Matrix rhs)
      throws DimensionMismatchException
  {
    if(getNumberOfRows() != rhs.getNumberOfRows() ||
       getNumberOfColumns() != rhs.getNumberOfColumns())
    {
      throw new DimensionMismatchException();
    }

    int i, j;
    for(i = 0; i < numberOfRows; ++i)
    {
      for(j = 0; j < numberOfColumns; ++j)
      {
        array[i][j] = array[i][j] + rhs.array[i][j];
      }
    }

    return this;
  } // plusEquals
  
  
  ////////////////////////////////////////////////////////////////////////////////
  // minus
  ////////////////////////////////////////////////////////////////////////////////
  
  /*!
   * \brief - operator implementation
   *
   * \param rhs Constant matrix to subract from the current matrix
   *
   * \return A constant matrix that is the difference of the two matrices
   *
   * \throws DimensionMismatchException if the number of rows in the current
   *    matrix does not match the number of rows in rhs or the number of columns
   *    in the current matrix does not match the number of columns in rhs
   */
  final Matrix minus(final Matrix rhs) 
      throws DimensionMismatchException
  {
    Matrix returnValue = this;
    returnValue.minusEquals(rhs);
    return returnValue;
  } // minus
  
  
  ////////////////////////////////////////////////////////////////////////////////
  // minusEquals
  ////////////////////////////////////////////////////////////////////////////////
  
  /*!
   * \brief -= operator implementation
   *
   * \param rhs Constant matrix to subract from and assign to the current matrix
   *
   * \return A reference to the current matrix after being subracted from
   *
   * \throws DimensionMismatchException if the number of rows in the current
   *    matrix does not match the number of rows in rhs or the number of columns
   *    in the current matrix does not match the number of columns in rhs
   */
  Matrix minusEquals(final Matrix rhs)
      throws DimensionMismatchException
  {
    if(getNumberOfRows() != rhs.getNumberOfRows() ||
       getNumberOfColumns() != rhs.getNumberOfColumns())
    {
      throw new DimensionMismatchException();
    }

    int i, j;
    for(i = 0; i < numberOfRows; ++i)
    {
      for(j = 0; j < numberOfColumns; ++j)
      {
        array[i][j] = array[i][j] - rhs.array[i][j];
      }
    }

    return this;
  } // operator plusEquals
  
  
  
  ////////////////////////////////////////////////////////////////////////////////
  // times
  ////////////////////////////////////////////////////////////////////////////////
  
  /*!
   * \brief * operator implementation
   *
   * \param rhs Constant matrix to multiply with the current matrix
   *
   * \return A constant matrix that is the product of the two matrices
   *
   * \throws DimensionMismatchException if the number of columns in the current
   *    matrix does not match the number of rows in rhs
   */
  final Matrix times(final Matrix rhs) 
      throws DimensionMismatchException, InvalidIndexValueException
  {
    Matrix returnValue = this;
    returnValue.timesEquals(rhs);
    return returnValue;
  } // times
  
  
  
  ////////////////////////////////////////////////////////////////////////////////
  // timesEquals
  ////////////////////////////////////////////////////////////////////////////////
  
  /*!
   * \brief *= operator implementation
   *
   * \param rhs Constant matrix to multiply with and assign to the current
   *    matrix
   *
   * \throws DimensionMismatchException if the number of columns in the current
   *    matrix does not match the number of rows in rhs
   */
  Matrix timesEquals(final Matrix rhs)
      throws DimensionMismatchException, InvalidIndexValueException
  {
    if(getNumberOfColumns() != rhs.getNumberOfRows())
    {
      throw new DimensionMismatchException();
    }

    Matrix copy = new Matrix(this);
   /* for(int i = 0; i < numberOfRows; ++i)
    {
      for(int j = 0; j < numberOfColumns; ++j)
      {
          System.out.println(copy.array[i][j]);
          
      }
    }*/

    //numberOfRows stays the same
    numberOfColumns = rhs.numberOfColumns;

    array = new double[numberOfRows][numberOfColumns];

    int i, j;
    for(i = 0; i < numberOfRows; ++i)
    {
      for(j = 0; j < numberOfColumns; ++j)
      {
          //System.out.println("The row is ");
          //copy.getRow(i + 1).printElements();
          //System.out.println("The col is ");
         // rhs.getColumn(j + 1).printElements();
        array[i][j] = VectorList.dotProduct(copy.getRow(i + 1),
                                            rhs.getColumn(j + 1));
        //System.out.println(array[i][j]);
      }
    }

    return this;
  } // timesEquals
  
  
  ////////////////////////////////////////////////////////////////////////////////
  // times
  ////////////////////////////////////////////////////////////////////////////////
  
  /*!
   * \brief * operator implementation
   *
   * \param rhs Constant vector to multiply with the current matrix
   *
   * \return A constant matrix that is the product of the current matrix with
   *    the vector passed in
   *
   * \throws DimensionMismatchException if the number of columns in the current
   *    matrix does not match the size of rhs
   */
  final VectorList times(final VectorList rhs) 
      throws DimensionMismatchException, InvalidIndexValueException
  {
    if(numberOfColumns != rhs.getSize())
    {
      throw new DimensionMismatchException();
    }
    VectorList returnValue = new VectorList(numberOfRows);
    int i;
    for(i = 1; i <= numberOfRows; ++i)
    {
      returnValue.setElement(i, VectorList.dotProduct(getRow(i), rhs));
    }
    return returnValue;
  } // operator*
  
  
  ////////////////////////////////////////////////////////////////////////////////
  // equalsEquals
  ////////////////////////////////////////////////////////////////////////////////
  
  /*!
   * \brief == operator implementation
   *
   * \param rhs Constant matrix to compare to the current matrix
   *
   * \return true if the two matrices have the same values in the same
   *    locations, otherwise false
   */
  boolean equalsEquals(final Matrix rhs) 
  {
    if(rhs.getNumberOfRows() != getNumberOfRows() ||
       rhs.getNumberOfColumns() != getNumberOfColumns())
    {
      return false;
    }

    int i, j;
    for(i = 0; i < numberOfRows; ++i)
    {
      for(j = 0; j < numberOfColumns; ++j)
      {
        if(array[i][j] != rhs.array[i][j])
        {
          return false;
        }
      }
    }

    return true;
  } // operator==
  
  
  ////////////////////////////////////////////////////////////////////////////////
  // notEquals
  ////////////////////////////////////////////////////////////////////////////////
  
  /*!
   * \brief != operator implementation
   *
   * \param rhs Constant matrix to compare to the current matrix
   *
   * \return true if the two matrices are different, otherwise false
   */
  boolean notEquals(final Matrix rhs) 
  {
    return !(this == rhs);
  } // operator!=
  
     
  ////////////////////////////////////////////////////////////////////////////////
  // applyScalar
  ////////////////////////////////////////////////////////////////////////////////
  
  /*!
   * \brief Apply a scalar to the current matrix.
   *
   * \param scalar The scalar to apply to the matrix
   */
  void applyScalar(double scalar)
  {
    int i, j;
    for(i = 0; i <= numberOfRows; ++i)
    {
      for(j = 0; j < numberOfColumns; ++j)
      {
        array[i][j] = array[i][j] * scalar;
      }
    }
  } // applyScalar

  
  ////////////////////////////////////////////////////////////////////////////////
  // applyScalar
  ////////////////////////////////////////////////////////////////////////////////
  
  /*!
   * \brief Apply a scalar to a matrix.
   *
   * \param scalar Scalar to be applied
   * \param m Constant matrix to apply the scalar to
   *
   * \return A Matrix that is m with the scalar applied to is
   */
  static Matrix applyScalar(double scalar, final Matrix m)
  {
    Matrix returnValue = new Matrix(m);
    returnValue.applyScalar(scalar);
    return returnValue;
  } // applyScalar
  
  
  
  ////////////////////////////////////////////////////////////////////////////////
  // getElement
  ////////////////////////////////////////////////////////////////////////////////
  
  /*!
   * \brief Get element located in the selected row and column
   *
   * \param row Row of the element
   *    where 1 <= row <= getNumberOfRows()
   * \param column Column of the element
   *    where 1 <= column <= getNumberOfColumns()
   * \return The value of the element selected
   * \throws InvalidIndexValueException if row < 1 or row > numberOfRows()
   *    or column < 1 or column > numberOfColumns()
   */
  double getElement(int row, int column) 
      throws InvalidIndexValueException
  {
    if(row < 1 || row > getNumberOfRows() ||
       column < 1 || column > getNumberOfColumns())
    {
      throw new InvalidIndexValueException();
    }
    return array[row - 1][column - 1];
  } // getElement

  
  
  ////////////////////////////////////////////////////////////////////////////////
  // transpose
  ////////////////////////////////////////////////////////////////////////////////
  
  /*!
   * \brief Transpose the current matrix.
   */
  void transpose()
  {
    double tempArray[][] = array;
    array = new double[numberOfColumns][numberOfRows];

    int i, j;
    for(i = 0; i < numberOfColumns; ++i)
    {
      for(j = 0; j < numberOfRows; ++j)
      {
        array[i][j] = tempArray[j][i];
      }
    }


    // Set the new number of rows/columns
    int tempNumber = numberOfRows;
    numberOfRows = numberOfColumns;
    numberOfColumns = tempNumber;
  } // transpose
  
  
  
  ////////////////////////////////////////////////////////////////////////////////
  // transpose
  ////////////////////////////////////////////////////////////////////////////////
  
  /*!
   * \brief Returns a matrix that is the transpose of the matrix passed in.
   *
   * \param m Constant Matrix to be transposed
   *
   * \return A Matrix that is the transpose of m
   */
  static Matrix transpose(final Matrix m)
  {
    Matrix returnValue = new Matrix(m);
    returnValue.transpose();
    return returnValue;
  } // transpose
  
  
  
  
  void printElements( ) throws InvalidIndexValueException{
      for(int i = 1; i<= this.numberOfRows; i++){
          for(int j =1; j<= this.numberOfColumns; j++){
          System.out.println(this.getElement(i,j));
      }
    }
  }
  
  
  
 public static void main(String args[]) throws InvalidIndexValueException, DimensionMismatchException{
     
      Matrix v1 = new Matrix(3,3,0);
      Matrix v2 = new Matrix(3,3,10);
      Matrix v3 = new Matrix(3,4,5);
      Matrix v4 = new Matrix(4,4,10);
      Matrix v5 = new Matrix(3,3);
      Matrix v6 = new Matrix(4,4);
     if(v1.equalsEquals(v5)){
         System.out.println("v1 and v2 are equal");
         v1.equals(v1.minus(v5));
         System.out.println("v1 and v5 are subtracted and the result is stored in v1");
         v1.printElements();
         v1.equals(v2.add(v5));
         System.out.println("v2 and v5 are added and the result is stored in v1");
         v1.printElements();
         v1.transpose();
         System.out.println("The transpose of the matrix is:");
         v1.printElements();
         v6.equals(v3.times(v4));//TODO Check times function
         System.out.println("The multiplication of the matrices is:");
         v6.printElements();
         //Todo Check other functions which are not tested so far 
         System.out.println("The timesEquals is:");
         Matrix v7 = new Matrix(3,3,5);
         v7.timesEquals(v2);
         v7.printElements();
         


     }
     else{
         System.out.println("Compared v1 and v6 are not equal");
     }
 }


    

}
