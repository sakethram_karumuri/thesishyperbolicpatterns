
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author karum006
 */
public class DrawingMouseListener implements MouseListener{

    public DrawingMouseListener() {
    }
    
    @Override
    public void mouseClicked(MouseEvent e){
        System.out.println("Mouse clicked. Coordinates are: " + e.getX()+ ", " + e.getY());
    }
    
    @Override
    public void mousePressed(MouseEvent e){
    }
    
    @Override
    public void mouseReleased(MouseEvent e){
    }
    
    @Override
    public void mouseEntered(MouseEvent e){
        System.out.println("Mouse entered. Coordinates are: " + e.getX()+ ", " + e.getY());
    }
    
    @Override
    public void mouseExited(MouseEvent e){
        System.out.println("Mouse exited. Coordinates are: " + e.getX()+ ", " + e.getY());
    }
}
