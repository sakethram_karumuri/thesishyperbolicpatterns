
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Vector;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author maneeshavejendla
 */
 public class PSFileCreator {
    
    static final String tab = "  ";
    static final String header = "%%!PS-Adobe-1.0\n"+
    "%%Prologue for point and line plotting\n" +
    "save 50 dict begin /context exch def\n" +
    "0 setlinewidth\n" +
    "0 setgray\n" +
    "1 setlinecap\n" +
    "/in {72 mul} def\n" +
    "/prx 4.25 in def\n" +
    "/pry 2.0 in def\n" +
    "/smtrx matrix currentmatrix def\n" +
    "/prmtrx prx pry translate matrix currentmatrix def\n" +
    "smtrx setmatrix\n" +
    "1.0 in 2.5 in translate\n" +
    "6.5 in dup scale\n" +
    "/dotsize 0.02 def\n" +
    "0.5 0.5 0.5 0 360 arc\n" +
    tab + "currentlinewidth\n" +
    tab + "0.002 setlinewidth\n" +
    tab + "stroke\n" +
    tab + "0.001 setlinewidth\n" +
    "newpath 0 0 moveto\n" +
    "/l {\n" +
    tab + "currentpoint\n" +
    tab + "newpath\n" +
    tab + "moveto\n" +
    tab + "lineto\n" +
    tab + "currentpoint\n" +
    tab + "stroke\n" +
    tab + "moveto\n" +
    "} def\n" +
    "/m {\n" +
    tab + "moveto\n" +
    "} def\n" +
    "/pg {\n" +
    tab + "newpath\n" +
    tab + "mark exch aload pop\n" +
    tab + "moveto counttomark 2 idiv\n" +
    tab + "{ lineto } repeat\n" +
    tab + "cleartomark\n" +
    tab + "eofill\n" +
    "} def\n" +
    // Define function name
    "/a {\n" +
    // put a2 on top of stack
    tab + "/a2 exch def\n" +
    // put a1 on top of stack
    tab + "/a1 exch def\n" +
    // put r on top of stack
    tab + "/r exch def\n" +
    // put yc on top of stack
    tab + "/yc exch def\n" +
    // put xc on top of stack
    tab + "/xc exch def\n" +
    // We need a newpath
    tab + "newpath\n" +
    // give the param. to the arc command
    tab + "xc yc r a1 a2 arc\n" +
    // draw the arc
    tab + "stroke\n" +
    // end function definition
    "} def\n" +
    // Define function name
    "/fa {\n" +
    // put a2 on top of stack
    tab + "/a2 exch def\n" +
    // put a1 on top of stack
    tab + "/a1 exch def\n" +
    // put r on top of stack
    tab + "/r exch def\n" +
    // put yc on top of stack
    tab + "/yc exch def\n" +
    // put xc on top of stack
    tab + "/xc exch def\n" +
    // We need a newpath
    tab + "newpath\n" +
    // give the param. to the arc command
    tab + "xc yc r a1 a2 arc\n" +
    // fill the arc
    tab + "fill\n" +
    // draw the arc
    tab + "stroke\n" +
    // end function definition
    "} def\n" +
    "/pl {\n" +
    tab + "newpath\n" +
    tab + "mark exch aload pop\n" +
    tab + "moveto counttomark 2 idiv\n" +
    tab + "{ lineto } repeat\n" +
    tab + "cleartomark\n" +
    tab + "stroke\n" +
    "} def\n" +
    "/wh {1.000 setgray} def\n" + // White
    "/bla {0.000 setgray} def\n" + // Black
    "/gry {0.500 setgray} def\n" + // Grey
    "/re {1 0 0 setrgbcolor} def\n" + // Red
    "/grn {0 1 0 setrgbcolor} def\n" + // Green
    "/blu {0 0 1 setrgbcolor} def\n" + // Blue
    "/cy {0 1 1 setrgbcolor} def\n" + // Cyan
    "/ma {1 0 1 setrgbcolor} def\n" + // Magenta
    "/ye {1 1 0 setrgbcolor} def\n" + // Yellow
    "/sal {1 0.5 0.4 setrgbcolor} def\n" + // Salmon
    "/brn {0.7 0.2 0.2 setrgbcolor} def\n"; //+ // Brown
    //"bla\n"); // Set the first color as black

    static final String footer = "copypage clear context end restore\n";
    
    File tempFile = new File("tmp.txt");//TODO Check this line again 
    
    
    ////////////////////////////////////////////////////////////////////////////////
    // Constructor
    ////////////////////////////////////////////////////////////////////////////////
    public PSFileCreator(File tempFile)
    {
        this.tempFile = tempFile;
      // Empty Implementation
    } // Constructor

    
    

    ////////////////////////////////////////////////////////////////////////////////
    // Copy Constructor
    ////////////////////////////////////////////////////////////////////////////////
    public PSFileCreator( final PSFileCreator p)
    {
      tempFile = new File (p.tempFile.getAbsolutePath());
    } // Copy Constructor
    
    
    
    
    ////////////////////////////////////////////////////////////////////////////////
    // operator equals
    ////////////////////////////////////////////////////////////////////////////////
    public PSFileCreator equals(final PSFileCreator rhs)
    {
      if(rhs != this)
      {
        tempFile = new File (rhs.tempFile.getAbsolutePath());
      }
      return this;
    } // operator equals
    
    
    
    
    
    ////////////////////////////////////////////////////////////////////////////////
    // addArc
    ////////////////////////////////////////////////////////////////////////////////
    public void addArc(final VectorList centerPoint, double radius,
                               double startAngle, double endAngle)
      throws InvalidIndexValueException, IOException
    {
      if(tempFile.canWrite() && tempFile.exists())
      {
        FileWriter fw = new FileWriter(tempFile);
        BufferedWriter out = new BufferedWriter(fw);
        if(centerPoint.getSize()< 2)
        {
          throw new InvalidIndexValueException();
        }
        String stringToBeFlushed =  new Double(centerPoint.getElement(1)).toString() + " " +
                                    new Double(centerPoint.getElement(2)).toString()+ " " +
                                    new Double(radius).toString() + " " +
                                    new Double(startAngle).toString() + " " +
                                    new Double(endAngle).toString() + " " +
                                    "a\n";
        out.write(stringToBeFlushed);
        out.flush();
        out.close();
      }
    } // addArc
    
    
    
    ////////////////////////////////////////////////////////////////////////////////
    // addFilledArc
    ////////////////////////////////////////////////////////////////////////////////
    public void addFilledArc(final VectorList centerPoint,
                                     double radius,
                                     double startAngle, double endAngle)
      throws InvalidIndexValueException, IOException
    {
      if(tempFile.canWrite() && tempFile.exists())
      {
        FileWriter fw = new FileWriter(tempFile);
        BufferedWriter out = new BufferedWriter(fw);
        if(centerPoint.getSize()< 2)
        {
          throw new InvalidIndexValueException();
        }
        String stringToBeFlushed =  new Double(centerPoint.getElement(1)).toString() + " " +
                                    new Double(centerPoint.getElement(2)).toString()+ " " +
                                    new Double(radius).toString() + " " +
                                    new Double(startAngle).toString() + " " +
                                    new Double(endAngle).toString() + " " +
                                    "fa\n";
        out.write(stringToBeFlushed);
        out.flush();
        out.close();
      }
    }// addFilledArc
    
    

    ////////////////////////////////////////////////////////////////////////////////
    // addLineTo
    ////////////////////////////////////////////////////////////////////////////////
    public void addLineTo(VectorList point)
      throws InvalidIndexValueException, IOException
    {
       if(tempFile.canWrite() && tempFile.exists())
      {
        FileWriter fw = new FileWriter(tempFile);
        BufferedWriter out = new BufferedWriter(fw);
        if(point.getSize()< 2)
        {
          throw new InvalidIndexValueException();
        }
        String stringToBeFlushed =  new Double(point.getElement(1)).toString() + " " +
                                    new Double(point.getElement(2)).toString()+ " " +
                                    "l\n";
        out.write(stringToBeFlushed);
        out.flush();
        out.close();
      }
    } // addLineTo

    ////////////////////////////////////////////////////////////////////////////////
    // addMoveTo
    ////////////////////////////////////////////////////////////////////////////////
    public void addMoveTo(VectorList point)
      throws InvalidIndexValueException, IOException
    {
       if(tempFile.canWrite() && tempFile.exists())
      {
        FileWriter fw = new FileWriter(tempFile);
        BufferedWriter out = new BufferedWriter(fw);
        if(point.getSize()< 2)
        {
          throw new InvalidIndexValueException();
        }
        String stringToBeFlushed =  new Double(point.getElement(1)).toString() + " " +
                                    new Double(point.getElement(2)).toString()+ " " +
                                    "m\n";
        out.write(stringToBeFlushed);
        out.flush();
        out.close();
      }
    } // addMoveTo
    
    
    ////////////////////////////////////////////////////////////////////////////////
    // addPolygon
    ////////////////////////////////////////////////////////////////////////////////
    void addPolygon(final Vector<VectorList> points)
      throws InvalidIndexValueException, IOException
    {
      if(tempFile.canWrite() && tempFile.exists())
      {
        FileWriter fw = new FileWriter(tempFile);
        BufferedWriter out = new BufferedWriter(fw);
        for( int i = 0; i < points.size(); ++i)
        {
          if(points.elementAt(i).getSize() < 2)
          {
            throw new InvalidIndexValueException();
          }
          if(0 == i)
          {
            // Add Start Mark
              out.write("[");
          }
          else
          {
            out.write(tab);
          }
          // Add normal part of the point
          out.write(new Double(points.elementAt(i).getElement(1)).toString() + " " +
                      new Double(points.elementAt(i).getElement(2)).toString() );
          if(points.size() - 1 == i)
          {
            // Add End
            out.write(" ] pg");
          }
          // Add newline no matter what
          out.write( "\n");
        }
        out.flush();
        out.close();
      }
    } // addPolygon
    
    
    
    ////////////////////////////////////////////////////////////////////////////////
    // addPolyline
    ////////////////////////////////////////////////////////////////////////////////
    void addPolyline(final Vector<VectorList > points)
      throws InvalidIndexValueException, IOException
    {
      if(tempFile.canWrite() && tempFile.exists())
      {
        FileWriter fw = new FileWriter(tempFile);
        BufferedWriter out = new BufferedWriter(fw);
        for(int i = 0; i < points.size(); ++i)
        {
          if(points.elementAt(i).getSize() < 2)
          {
            throw new InvalidIndexValueException();
          }
          if(0 == i)
          {
            // Add Start Mark
            out.write("[ ");
          }
          else
          {
            out.write(tab);
          }
          // Add normal part of the point
          out.write(new Double(points.elementAt(i).getElement(1)).toString() + " " +
                      new Double(points.elementAt(i).getElement(2)) );
          if(points.size() - 1 == i)
          {
            // Add End
            out.write(" ] pl");
          }
          // Add newline no matter what
          out.write("\n");
        }
        out.flush();
        out.close();

      }
    } // addPolyline
    
    
    
    
    ////////////////////////////////////////////////////////////////////////////////
    // changeColor
    ////////////////////////////////////////////////////////////////////////////////
    void changeColor(DefinedColor colorEnum) throws IOException
    {
       if(tempFile.canWrite() && tempFile.exists())
      {
        FileWriter fw = new FileWriter(tempFile);
        BufferedWriter out = new BufferedWriter(fw);
        DefinedColor Color = colorEnum;
        switch(Color)
        {
        case COLOR_BLACK:
          out.write("bla");
          break;
        case COLOR_WHITE:
          out.write("wh");
          break;
        case COLOR_GREY:
          out.write("gry");
          break;
        case COLOR_RED:
          out.write("re");
          break;
        case COLOR_GREEN:
          out.write("grn");
          break;
        case COLOR_BLUE:
          out.write("blu");
          break;
        case COLOR_CYAN:
          out.write("cy");
          break;
        case COLOR_MAGENTA:
          out.write("ma");
          break;
        case COLOR_YELLOW:
          out.write("ye");
          break;
        case COLOR_SALMON:
          out.write("sal");
          break;
        case COLOR_BROWN:
          out.write("brn");
          break;
        }
        out.write("\n");
        out.flush();
        out.close();
      }
    } // changeColor
    
    ////////////////////////////////////////////////////////////////////////////////
    // finalize
    ////////////////////////////////////////////////////////////////////////////////
    void finalizeFromQT() throws IOException
    {
      if(tempFile.canWrite() && tempFile.exists())
      {
        FileWriter fw = new FileWriter(tempFile);
        BufferedWriter out = new BufferedWriter(fw);
        out.write(footer);
        out.flush();
        out.close();
      }
    } // finalize
    
    
    ////////////////////////////////////////////////////////////////////////////////
    // initialize
    ////////////////////////////////////////////////////////////////////////////////
    void PSFileCreatorinitialize() throws IOException
    {
      int temp = 0;
      String tempFileNamePrefix = "temp";
      
      File tempFile = new File(tempFileNamePrefix+new Integer(temp).toString());
      while(tempFile.exists())
      {
        ++temp;
        tempFile = new File(tempFileNamePrefix+new Integer(temp).toString());     
      }
      this.tempFile = new File(tempFile.getAbsolutePath());
      
      if(tempFile.canWrite() && tempFile.exists())
      {
        FileWriter fw = new FileWriter(tempFile);
        BufferedWriter out = new BufferedWriter(fw);
        out.write(header);
        out.flush();
        out.close();
      }
    } // initialize
    
    ////////////////////////////////////////////////////////////////////////////////
    // translatePoint
    ////////////////////////////////////////////////////////////////////////////////
    static VectorList translatePoint(final VectorList point) throws InvalidIndexValueException
    {
      if(point.getSize() < 2)
      {
        throw new InvalidIndexValueException();
      }
      VectorList returnValue = new VectorList(2);
      returnValue.setElement(1, (point.getElement(1) + 1.0) / 2.0);
      returnValue.setElement(2, (point.getElement(2) + 1.0) / 2.0);
      return returnValue;
    } // translatePoint
    
    
    
    
    
    
    ////////////////////////////////////////////////////////////////////////////////
    // writeFile
    ////////////////////////////////////////////////////////////////////////////////
    
    boolean writeFile(final String fileName,
                                  boolean deleteTempFile)
    {
      File file = new File(fileName);
      boolean returnValue = false;
      if(file.canWrite() && file.exists() &&
         tempFile.exists() && tempFile.canRead())
      {
        try
        {
          FileWriter fw = new FileWriter(file);
          BufferedWriter out = new BufferedWriter(fw);
          FileReader fr = new FileReader(tempFile);
          BufferedReader in = new BufferedReader(fr);
          String line = null;
          while((line = in.readLine()) != null) {
            out.write(line);         
          }	
          
          out.flush();
          out.close();
          in.close();
          returnValue = true;
          if(deleteTempFile)
          {
            tempFile.delete();
          }
        }
        catch(Exception e)
        {
          // Empty Implementation
        }
      }

      return returnValue;
    } // writeFile

public static void main(String[] args) throws IOException{
    File file = new File("newfile.txt");
 
    if (file.createNewFile()){
      System.out.println("File is created!");
    }else{
      System.out.println("File already exists. And the location is");
      String pathOfFile  = file.getAbsolutePath();
      System.out.println(pathOfFile);
    }
    
    System.out.println("Testing the constructor with file in it");     
    PSFileCreator p = new PSFileCreator(file);
    System.out.println("Testing the constructor with PSCreator in it");     
    PSFileCreator p2 = new PSFileCreator(p);
    System.out.println("Testing the constructor with PSCreator in it");         
    File file2 = new File("AnotherNewfile.txt");
    PSFileCreator p3 = new PSFileCreator(file2);
    System.out.println("Testing the equals function");         
    p3.equals(p2);

    
    p.PSFileCreatorinitialize();
    
}




 }