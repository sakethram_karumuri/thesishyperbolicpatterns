/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author maneeshavejendla
 */
public class DrawingUtility extends Utility{
    
    ////////////////////////////////////////////////////////////////////////////////
    // hyperblicLine
    ////////////////////////////////////////////////////////////////////////////////
    
    /*!
    * \brief A utility function to help calculate hyperbolic lines
    *
    * \param startPoint The starting point as a 2-dimensional vector
    * \param endPoint The ending point as a 2-dimensional vector
    *
    * \return A 3-dimensional vector that is the cross product of starting point
    *         and ending point after the points have been stereofied
    * \throws InvalidIndexValueException if startPoint.getSize() < 2 or
    *         endPoint.getSize() < 2
    */
    
    static VectorList hyperbolicLine(final VectorList startPoint,
                                                  final VectorList endPoint)
      throws InvalidIndexValueException
    {
      VectorList startStereo = new VectorList(Utility.stereofyPoint(startPoint));
      VectorList endStereo = new VectorList(Utility.stereofyPoint(endPoint));
      VectorList returnValue = new VectorList(3);
      double start1 = startStereo.getElement(1);
      double start2 = startStereo.getElement(2);
      double start3 = startStereo.getElement(3);
      double end1 = endStereo.getElement(1);
      double end2 = endStereo.getElement(2);
      double end3 = endStereo.getElement(3);
      // Old code does not do a true cross product, so must do manually:
      returnValue.setElement(1, start2 * end3 - start3 * end2);
      returnValue.setElement(2, start3 * end1 - start1 * end3);
      returnValue.setElement(3, start2 * end1 - start1 * end2);

      double element1Squared = returnValue.getElement(1);
      element1Squared *= element1Squared;

      double element2Squared = returnValue.getElement(2);
      element2Squared *= element2Squared;

      double element3Squared = returnValue.getElement(3);
      element3Squared *= element3Squared;

      double norm = Math.sqrt(element1Squared + element2Squared - element3Squared);
      returnValue.applyScalar(1.0 / norm);
      return returnValue;
    } // hyperbolicLine

    ////////////////////////////////////////////////////////////////////////////////
    // resizeHyperbolicPoint
    ////////////////////////////////////////////////////////////////////////////////
    /*!
    * \brief A utility function to resize a hyperblic point to the Java Applet //To Do Check again
    *        coordinate system
    *
    * \param point The point as a 2-dimensional vector (x in element 1 and y in
    *        element 2)
    * \param sceneSize the size of the scene which is used for scaling
    *
    * \return The resized point (x in element 1 and y in element 2)
    *
    * \throws InvalidIndexValueException if point.getSize() < 2
    */
    
    static VectorList resizeHyperbolicPoint(
        final VectorList point,
        int sceneSize)
      throws InvalidIndexValueException
    {
      if(point.getSize() < 2)
      {
        throw new InvalidIndexValueException();
      }
      VectorList returnValue = new VectorList(point);
      returnValue.applyScalar(sceneSize);
      return returnValue;
    } // resizeHyperoblicPoint

    
    public static void main(String args[]) throws InvalidIndexValueException {
        DrawingUtility d = new DrawingUtility();
        VectorList v1 = new VectorList(2,4.0);
        VectorList v2 = new VectorList(2,3.0);
        VectorList v3 = d.hyperbolicLine(v1, v2);
        v3.printElements();
        VectorList v4 = d.resizeHyperbolicPoint(v1,5);
        v4.printElements();

    }
    
}
