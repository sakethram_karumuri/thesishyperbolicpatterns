/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author maneeshavejendla
 */
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.*;
import javax.swing.*;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;


public class MenuApplet extends JApplet implements ActionListener, MenuListener, KeyListener{
    JMenuBar menubar;
    JMenu file, edit, tools;
    JMenuItem newFile, open, save, exit, settings ;
    JMenuItem levelOne, levelTwo, levelThree, levelFour, levelFive, levelSix, levelSeven;
    
    JLabel response;
    File  openfile;
    JFileChooser fc = null;
    BufferedReader br;
    String currentFileBeingEdited = null, line = null;
    
    
    JPanel rightside;             //Panel to hold controls
    Canvas leftside;             //Canvas for drawing
    JTextArea status;            //Status area

    @Override
    public void init(){
        
       
        // Initialize the menu
        initializeMenu();
        
        
         //TODO group them as boxes and label them
        setLayout(new BorderLayout(10,10));
        setFont(new Font("TimesRoman",Font.PLAIN,12));

        //setLayout(new GridLayout(1,2));
        leftside = new Canvas();          //Canvas instantiated
        rightside = new JPanel();          //Panel instantiated
        
        //TODO Change the sizes of the left and right parts
        leftside.setSize((getSize().width)/2,getSize().height);
        rightside.setSize(getSize().width/2,getSize().height);
        add(leftside);       //elements are added to the applet
        add(rightside);
        
        
        rightside.setLayout(new GridLayout(2,1));
        //control panel is divided into 3 parts
        JPanel top = new JPanel();
        status = new JTextArea(5, 20);
        

        top.setLayout(new GridLayout(3,1));
        JPanel layers = new JPanel();
        JPanel zoom  =  new JPanel();
        JPanel points = new JPanel();
        
        
        //Choice of number of layers TODO make it a spinbox
        Choice choice = new Choice();
        for(int i = 1; i< 10; i++){
            choice.addItem("Choice "+i);
        }    
        layers.add(choice); 
        //Add layers Button
        JButton LayerChanges = new JButton("Apply layer Changes");
        layers.add(LayerChanges);
        top.add(layers);      
        
        
        
        //Choice of Zoom TODO make it a spinbox
        Choice zoomChoice = new Choice();
        for(int i = 1; i< 10; i++){
            zoomChoice.addItem("Choice "+i);
        }      
        zoom.add(zoomChoice);
        //Add Zoom Button
        JButton zoomButton = new JButton("Zoom");
        zoom.add(zoomButton);
        top.add(zoom); 
        
        
        
        //TODO Something wrong with the points
        //Adding points buttons
        points.setLayout(new GridLayout(1,2));
        JTextArea pointsText = new JTextArea(5,5);
        JPanel pointButtons = new JPanel();

        pointsText.setLayout(new GridLayout(1,1));
        pointsText.setEditable(false);
        points.add(pointsText);

        pointButtons.setLayout(new GridLayout(4,1));
        JButton moveUp = new JButton("Move Point Group Up");
        JButton moveDown = new JButton("Move Point Group Down");
        JButton modify = new JButton("Modify Point Group");
        JButton delete = new JButton("Delete Point or Point Group");
        pointButtons.add(moveUp);
        pointButtons.add(moveDown);
        pointButtons.add(modify);
        pointButtons.add(delete);
        points.add(pointButtons);
        top.add(points); 
        

        rightside.add(top);
        
        status.setLayout(new GridLayout(1,1));
        status.setEditable(false);
        status.setWrapStyleWord(false);
        status.setSize(200, 400);
        status.setName("Status");
        rightside.add(status);

                
        
    }
    
    
    // TODO Add actions for all the menu items
    public void initializeMenu(){
        
       
        menubar = new JMenuBar();
        
        file = new JMenu("File");
        //TODO short cut not working
        file.setMnemonic(KeyEvent.VK_F);
        edit = new JMenu("Edit");
        edit.setMnemonic(KeyEvent.VK_E);
        tools = new JMenu("Tools");
        tools.setMnemonic(KeyEvent.VK_T);
        menubar.add(file);
        menubar.add(edit);
        menubar.add(tools);
        
        this.addKeyListener(this);
        
        newFile = new JMenuItem("New");
        newFile.setMnemonic(KeyEvent.VK_N); 
        newFile.addActionListener(this);
        
        open = new JMenuItem("Open");
        open.setMnemonic(KeyEvent.VK_O);
        open.addActionListener(this);

        save = new JMenuItem("Save");
        save.setMnemonic(KeyEvent.VK_S);
        save.addActionListener(this);
  
        exit = new JMenuItem("eXit");
        exit.setMnemonic(KeyEvent.VK_X);
        exit.addActionListener(this);
        
        file.add(newFile);
        file.add(open);
        file.add(save);
        file.add(exit);
        
        
        levelOne = new JMenuItem("Add Move to Point");
        levelTwo = new JMenuItem("Add Draw to Point");
        levelThree = new JMenuItem("Add Circle");
        levelFour = new JMenuItem("Add Filled Circle");
        levelFive = new JMenuItem("Add Filled Polygon");
        levelSix = new JMenuItem("Add Filled Pgon");
        levelSeven = new JMenuItem("Add Polyline");
        edit.add(levelOne);
        edit.add(levelTwo);
        edit.add(levelThree);
        edit.add(levelFour);
        edit.add(levelFive);
        edit.add(levelSix);
        edit.add(levelSeven);
        
        
        settings = new JMenuItem("Settings");
        tools.add(settings);
        
        setJMenuBar(menubar);
        
    }
    
    public void newFile(){
        
        
        
        
        
    }
    
    
    public void openFile(){
        
        fc = new JFileChooser();
        // directories only to be selected
        fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
        fc.setSelectedFile(fc.getCurrentDirectory());
        fc.setDialogTitle("Directory Chooser");
        fc.setMultiSelectionEnabled(false);

        int retVal = fc.showOpenDialog(MenuApplet.this);

        if (retVal == JFileChooser.APPROVE_OPTION) {
            //Clearing the contents of the text area
            status.setText("");
            openfile = fc.getSelectedFile();
            currentFileBeingEdited = openfile.getAbsolutePath();
            try {
                br = new BufferedReader(new FileReader(openfile));
            } catch (FileNotFoundException e1) {
            }
            try {
                line = br.readLine();
            } catch (IOException e1) {
            }
            while (line != null) {
                try {
                    line = br.readLine();
                    System.out.println(line);
                } catch (IOException e1) {
                }
            }
        }
      
   }

    public void menuSelected(MenuEvent e) {
        throw new UnsupportedOperationException("Not supported yet.");
        
    }

    public void menuDeselected(MenuEvent e) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void menuCanceled(MenuEvent e) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void keyTyped(KeyEvent e) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void keyPressed(KeyEvent e) {
        //throw new UnsupportedOperationException("Not supported yet.");
        if(e.getKeyChar()=='X'){
            System.exit(0);
        }
    }

    public void keyReleased(KeyEvent e) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void actionPerformed(ActionEvent e) {
        
        String menuName = e.getActionCommand();
        if(e.getSource().equals(newFile)){
            newFile();
        }
        else if(e.getSource().equals(open)){
            openFile();
            
        }else if(e.getSource().equals(save)){
            
            PrintWriter pw = null;
            try {
                pw = new PrintWriter(new File(currentFileBeingEdited));
                //pw.println(ta.getText());
            } catch (FileNotFoundException e1) {
                e1.printStackTrace();
            } finally {
                if(pw != null){
                    pw.close();
                }
            }
            
        }else if(e.getSource().equals(exit)){
            
            System.exit(0);
            
        }else{
            response.setText("Menu Item '" + menuName + "' is selected.");

        }
    }
    
}