
public class HorocycleUtil {
	
	
	public static VectorList rotatePointClockwise(VectorList point) throws InvalidIndexValueException, DimensionMismatchException{
		  Matrix m = new Matrix(3,1,1);
		  double x = point.getElement(1);
		  double y = point.getElement(2);
	      m.setElement(1, 1, x);
	      m.setElement(2, 1, y);
	      //m.printElements();
	      
	      double costheta = x/Math.sqrt(x*x + y*y);
	      double sintheta = y/Math.sqrt(x*x + y*y);
	      
	      Matrix rotationMat = new Matrix(3,3,0);
	      rotationMat.setElement(1, 1, costheta);
	      rotationMat.setElement(1, 2, sintheta);
	      rotationMat.setElement(2, 1, -sintheta);
	      rotationMat.setElement(2, 2, costheta);
	      rotationMat.setElement(3, 3, 1);
	      
	      Matrix newM = rotationMat.times(m);
	      VectorList newPoint = new VectorList(2);
	      newPoint.setElement(1, newM.getElement(1, 1));
	      newPoint.setElement(2, newM.getElement(2, 1));
	      
	      return newPoint;
		  
	  }
	  
	  public static VectorList rotatePointAntiClockwise(VectorList point1, VectorList point2) throws InvalidIndexValueException, DimensionMismatchException{
		  Matrix m = new Matrix(3,1,1);
		  double x = point2.getElement(1);
		  double y = point2.getElement(2);
	      m.setElement(1, 1, x);
	      m.setElement(2, 1, y);
	      System.out.println();
	      //m.printElements();
	      
	      double x1 = point1.getElement(1);
	      double y1 = point1.getElement(2);
	      double costheta = x1/Math.sqrt(x1*x1 + y1*y1);
	      double sintheta = y1/Math.sqrt(x1*x1 + y1*y1);
	      
	      Matrix rotationMat = new Matrix(3,3,0);
	      rotationMat.setElement(1, 1, costheta);
	      rotationMat.setElement(1, 2, -sintheta);
	      rotationMat.setElement(2, 1, sintheta);
	      rotationMat.setElement(2, 2, costheta);
	      rotationMat.setElement(3, 3, 1);
	      System.out.println();
	      //rotationMat.printElements();
	 
	      Matrix newM = rotationMat.times(m);
	      VectorList newPoint = new VectorList(2);
	      newPoint.setElement(1, newM.getElement(1, 1));
	      newPoint.setElement(2, newM.getElement(2, 1));
	      
	      return newPoint;
		  
	  }
	  
	  public static VectorList normalize(VectorList point) throws InvalidIndexValueException, DimensionMismatchException
	  {
		  VectorList newPoint = new VectorList(2);
		  
		  double x = point.getElement(1);
		  double y = point.getElement(2);
	            
	      newPoint.setElement(1, x/Math.sqrt(x*x + y*y));
	      newPoint.setElement(2, y/Math.sqrt(x*x + y*y));
	      
	      return newPoint;
	  }
	  
	  
	  public static void main(String args[]) throws InvalidIndexValueException, DimensionMismatchException
	  {
		     
	      VectorList point = new VectorList(2);
	      point.setElement(1, 0.55);
	      point.setElement(2, 0.55);
	      VectorList newP = rotatePointClockwise(point);
	      System.out.println(newP.getElement(1) + " " + newP.getElement(2));
	      VectorList backnewP = rotatePointAntiClockwise(point, newP);
	      System.out.println(backnewP.getElement(1) + " " + backnewP.getElement(2));
	      
	      VectorList normalizedPoint = normalize(point);
	      System.out.println(normalizedPoint.getElement(1) + " " + normalizedPoint.getElement(2));
	      	      
	  }

}
