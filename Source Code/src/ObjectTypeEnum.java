/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author maneeshavejendla
 */


/*!
 * \enum ObjectType
 *
 * \brief Object Types
 */
enum ObjectType
{
  OBJECT_TYPE_MOVE_TO (1),
  OBJECT_TYPE_DRAW_TO (2),
  OBJECT_TYPE_CIRCLE (3),
  OBJECT_TYPE_FILLED_POLYGON (4),
  OBJECT_TYPE_HYPERLINE (7),
  OBJECT_TYPE_FILLED_CIRCLE (8),
  OBJECT_TYPE_POLYLINE (9),
  OBJECT_TYPE_FILLED_PGON (12),
  OBJECT_TYPE_EQUIDISTANT_CURVE (20),
  OBJECT_TYPE_HOROCYCLE (21),
  OBJECT_TYPE_HOROCYCLE_LINES (22),
  OBJECT_TYPE_HYPERBOLIC_LINESEGMENT(23),
  OBJECT_TYPE_HYPERBOLIC_LINE(24);
  
   private int enumVal;
    
   ObjectType(int enumVal ){
        this.enumVal = enumVal;
   }
    
    int getObjectTypeEnumVal(){
        return enumVal;
    }
    
  
} // PointType

public class ObjectTypeEnum {
     
    ObjectType type;
    
    ObjectTypeEnum(){
        
    }
    ObjectTypeEnum(ObjectType type){
        this.type = type;
    }

    
    ////////////////////////////////////////////////////////////////////////////////
    // objectTypeToString
    ////////////////////////////////////////////////////////////////////////////////
    static String objectTypeToString(ObjectType type)
    {
      String returnString = null;
      switch(type)
      {
      case OBJECT_TYPE_MOVE_TO:
        returnString =  "Move To";
        break;
      case OBJECT_TYPE_DRAW_TO:
        returnString =  "Draw To";
        break;
      case OBJECT_TYPE_CIRCLE:
        returnString =  "Circle";
        break;
      case OBJECT_TYPE_FILLED_POLYGON:
        returnString =  "Filled Polygon";
        break;
      case OBJECT_TYPE_HYPERLINE:
        returnString =  "Hyperline";
        break;
      case OBJECT_TYPE_FILLED_CIRCLE:
        returnString =  "Filled Circle";
        break;
      case OBJECT_TYPE_POLYLINE:
        returnString =  "Polyline";
        break;
      case OBJECT_TYPE_FILLED_PGON:
        returnString =  "Filled Pgon";
        break;
      case OBJECT_TYPE_HYPERBOLIC_LINESEGMENT:
          returnString =  "Hyperbolic Line Segment";
          break;
      case OBJECT_TYPE_HYPERBOLIC_LINE:
          returnString =  "Hyperbolic Line";
          break;
      case OBJECT_TYPE_EQUIDISTANT_CURVE:
          returnString =  "Equidistant Curve";
          break;
      case OBJECT_TYPE_HOROCYCLE:
          returnString =  "Horocycle";
          break;
      case OBJECT_TYPE_HOROCYCLE_LINES:
          returnString =  "Horocycle With Lines";
          break;
      default:
        returnString =  "Unknown Type";
        break;
      }
      return returnString;
    } // objectTypeToString
    
    
    
    public static void main(String[] args){
        
        
        System.out.println("testing this Enum class");
        ObjectTypeEnum o = new ObjectTypeEnum();
        String testString = o.objectTypeToString(ObjectType.OBJECT_TYPE_CIRCLE);
        System.out.println("The tested string is :"+testString);
        String testString2 = o.objectTypeToString(ObjectType.OBJECT_TYPE_DRAW_TO);
        System.out.println("The tested string is :"+testString2);
        

        
        
        
    }


    
}
