/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author maneeshavejendla
 */
class VectorList{
    
    double array[];
    int size;
    
     public VectorList(int numberOfElements, double initValue)
      throws InvalidIndexValueException
  {
      
    if(numberOfElements > 0)
    {
      size = numberOfElements;
      int i;
      array = new double[size];
      for(i = 0; i < size; ++i)
      {
        array[i] = initValue;
      }
    }
    else
    {
      throw(new InvalidIndexValueException());
    }
  } // Constructor
     
     //TODO the init value was intialized to 0 in c++ code
     public VectorList(int num) throws InvalidIndexValueException{
         this(num,0);
     }
     
    public VectorList(final VectorList v)
    {
      size = v.size;
      array = new double[size];
      int i;
      for(i = 0; i < size; ++i)
      {
        array[i] = v.array[i];
      }
    } // Copy Constructor
    
    
   VectorList equals(final VectorList rhs)
  {
    if(rhs != this)
    {
      array = null;
      size = rhs.size;
      array = new double[size];
      int i;
      for(i = 0; i < size; ++i)
      {
        array[i] = rhs.array[i];
      }
    }
    return this;
  } // operator equals
   
   
   /*!
   * \brief Overload the + operator
   *
   * If the sizes of the two vectors don't match, the difference in size is
   * assumed to contain 0's
   *
   * \param rhs Constant vector to add to the current vector
   *
   * \return A constant vector that is the sum of the two vectors
   */
   //TODO make the function immutable
  final VectorList add(final VectorList rhs) 
  {
    VectorList returnValue = this;
    returnValue.plusEquals(rhs);
    return returnValue;
  } // operator add

    /*!
   * \brief Overload the += operator
   *
   * If the sizes of the two vectors don't match, the difference in size is
   * assumed to contain 0's
   *
   * \param rhs Constant vector to add and assign to the current vector
   *
   * \return A reference to the current vector after being added to
   */
  VectorList plusEquals(final VectorList rhs)
  {
    int i;
    int tempSize = size;
    if(size < rhs.size)
    {
      // Need to copy and enlarge
      double tempArray[] = array;
      size = rhs.size;
      array = new double[size];

      for(i = 0; i < tempSize; ++i)
      {
        array[i] = tempArray[i];
      }
      tempArray = null;
    }
    // If size > rhs.size, only need to mess with i < rhs.size, otherwise addition
    // only takes place for i < tempSize elements, others are just assignments
    int maxIndex = tempSize < rhs.size ?tempSize:rhs.size;
    for(i = 0; i < maxIndex; ++i)
    {
      // Both arrays have the elements
      array[i] = array[i] + rhs.array[i];
    }
    for(; i < rhs.size; ++i)
    {
      // Ran out of elements in the original vector, just copy the elements from
      // the other vector
      array[i] = rhs.array[i];
    }
    return this;
  } // plusEquals
    
  
  /*!
   * \brief Overload the - operator
   *
   * If the sizes of the two vectors don't match, the difference in size is
   * assumed to contain 0's
   *
   * \param rhs Constant vector to subract from the current vector
   *
   * \return A constant vector that is the difference of the two vectors
   */
  final VectorList minus(final VectorList rhs) 
  {
    VectorList returnValue = this;
    returnValue.minusEquals(rhs);
    return returnValue;
  } // minus
  
  
  
 /*!
   * \brief Overload the -= operator
   *
   * If the sizes of the two vectors don't match, the difference in size is
   * assumed to contain 0's
   *
   * \param rhs Constant vector to subtract from and assign to the current
   *    vector
   *
   * \return A reference to the current vector after being subracted from
   */
  VectorList minusEquals(final VectorList rhs)
  {
    int i;
    int tempSize = size;
    if(size < rhs.size)
    {
      // Need to copy and enlarge
        double tempArray[] = array;
      size = rhs.size;
      array = new double[size];

      for(i = 0; i < tempSize; ++i)
      {
        array[i] = tempArray[i];
      }
       tempArray = null;
    }
    // If size > rhs.size, only need to mess with i < rhs.size, otherwise
    // addition only takes place for i < tempSize elements, others are just
    // assignments
    int maxIndex = tempSize < rhs.size ?tempSize:rhs.size;
    for(i = 0; i < maxIndex; ++i)
    {
      // Both arrays have the elements
      array[i] = array[i] - rhs.array[i];
    }
    for(; i < rhs.size; ++i)
    {
      // Ran out of elements in the original vector, just copy the elements from
      // the other vector
      array[i] = rhs.array[i] * -1;
    }
    return this;
  } // minusEquals
    
  
  
  
   /*!
   * \brief Overload the == operator
   *
   * \param rhs Constant vector to compare to the current vector
   *
   * \return true if the two vectors have the same values in the same locations,
   *    otherwise false
   */
  boolean equalsEquals(final VectorList rhs) 
  {
    if(size != rhs.size)
    {
      return false;
    }

    int i;
    for(i = 0; i < size; ++i)
    {
      if(array[i] != rhs.array[i])
      {
        return false;
      }
    }

    return true;
  } // equalsEquals
  
  
  
  /*!
   * \brief Overload the != operator
   *
   * \param rhs Constant vector to compare to the current vector
   *
   * \return true if the two vectors are different, otherwise false
   */
  boolean notEquals(final VectorList rhs) 
  {
    return !(this.equalsEquals(rhs));
  } // notEquals
  
  
    /*!
   * \brief Apply a scalar to the current vector
   *
   * \param scalar The scalar to apply to the vector
   */
  void applyScalar(double scalar)
  {
    int i;
    for(i = 0; i < size; ++i)
    {
      array[i] = array[i] * scalar;
    }
  } // applyScalar
  
  
   /*!
   * \brief Apply a scalar to a vector
   *
   * \param v Vector to apply the scalar to
   * \param scalar The scalar to apply to the vector
   *
   * \return A vector with the scalar applied to it
   */
  static VectorList applyScalar(final VectorList v, double scalar)
  {
    VectorList returnValue = v;
    returnValue.applyScalar(scalar);
    return returnValue;
  } // applyScalar

  
   /*!
   * \brief Calculate the cross product of two vectors
   *
   * Both vectors must have size 3, otherwise a DimensionMismatch exception will
   * be thrown. Calculation is done as a x b.
   *
   * \param a First vector
   * \param b Second vector
   *
   * \return The cross product of the two vectors
   *
   * \throws DimensionMismatchException if a.size != 3 or b.size != 3
   */
  static VectorList crossProduct(final VectorList a, final VectorList b)
      throws DimensionMismatchException, InvalidIndexValueException
  {
    if(a.size != 3 || b.size != 3)
    {
      throw new DimensionMismatchException();
    }
    VectorList returnValue = new VectorList(3);
    returnValue.array[0] = a.array[1] * b.array[2] -
                           a.array[2] * b.array[1];
    returnValue.array[1] = a.array[2] * b.array[0] -
                           a.array[0] * b.array[2];
    returnValue.array[2] = a.array[0] * b.array[1] -
                           a.array[1] * b.array[0];
    return returnValue;
  } // crossProduct
  
  
  
  /*!
   * \brief Calculate the dot product of two vectors
   *
   * If the sizes of the two vectors don't match, the difference in size is
   * assumed to contain 0's
   *
   * \param a First vector
   * \param b Second vector
   *
   * \return The dot product of the two vectors
   */
  static double dotProduct(final VectorList a, final VectorList b)
  {
    double returnValue = 0.0;
    int maxIndex = a.size<b.size?a.size:b.size;
    int i;
    for(i = 0; i < maxIndex; ++i)
    {
      returnValue = returnValue + a.array[i] * b.array[i];
    }
    return returnValue;
  } // dotProduct

  
  
  
  /*!
   * \brief Get an element of the vector
   *
   * \param index The index of the element to get
   *
   * \return The element specified
   *
   * \throws InvalidIndexValueException if the index specified is < 1 or > size
   */
  double getElement(int index) 
      throws InvalidIndexValueException
  {
    if(index < 1 || index > size)
    {
      throw new InvalidIndexValueException();
    }
    return array[index - 1];
  } // getElement
  
  
  
  /*!
   * \brief Get the size of the vector
   *
   * \return The size of the vector
   */
  int getSize() 
  {
    return size;
  } // getSize
  
  
   /*!
   * \brief Set an element of the array
   *
   * \param index The index of the element to set
   * \param value The value to set the element at index to
   *
   * \throws InvalidIndexValueException if the index specified is < 1 or > size
   */
  void setElement(int index, double value)
      throws InvalidIndexValueException
  {
    if(index < 1 || index > size)
    {
      throw new InvalidIndexValueException();
    }
    array[index - 1] = value;
  } // setElement
  
  
  void printElements( ) throws InvalidIndexValueException{
      for(int i = 1; i<= this.size; i++){
          System.out.println(this.getElement(i));
      }
  }
  
  
  public static void main(String args[]) throws InvalidIndexValueException, DimensionMismatchException{
      
      VectorList v1 = new VectorList(10,2.0);
      VectorList v2 = new VectorList(10,3.0);
      VectorList v3 = new VectorList(3,4.0);
      VectorList v4 = new VectorList(3,5.0);
      VectorList v5 = new VectorList(3);
      VectorList v6 = new VectorList(3);
      if(v5.equalsEquals(v6)){
          System.out.println("Two vectorllists v5 & v6 are equal");
          v5.equals(v1.add(v4));         
          System.out.println("v1 is added to v4 and is assigned to v5");
          System.out.println("Printing the elements of v5 after the addition ");          
          v5.printElements();
          v6.equals(v4.add(v3));
          System.out.println("v3 is substracted from v4 and is assigned to v6");
          System.out.println("Printing the elements of v6 after the subtraction ");          
          v5.printElements();

      }
      else if(v1.notEquals(v6)){
          System.out.println("Given vectors v1 & v6 are not equal");
      }
      
      System.out.println("Printing the elements of v3 now after multiplying the scalar");          
      v3.applyScalar(10);
      v3.printElements();
      VectorList crossp = new VectorList(5);
      crossp.crossProduct(v3,v4);
      System.out.println("The result of the cross product is ");
      crossp.printElements();
      VectorList v9 = new VectorList(3,4.0);
      VectorList v10 = new VectorList(3,5.0);
      int dotPro = (int) dotProduct(v9,v10);
      System.out.println("The result of the dot product is ");
      System.out.println(dotPro);
      
  }

    
    
}
