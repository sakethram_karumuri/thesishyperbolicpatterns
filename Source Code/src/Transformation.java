/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author maneeshavejendla
 */

/*!
 * \struct Transformation
 *
 * \brief Structure to represent a transformation
 * 
 */
 import java.util.Vector; 

public class Transformation {

    
  Matrix matrix;
  int pPosition;
  int orientation;
  Vector<Integer> colorPermutation;
  int fileOrientation;
  
  
    /*!
   * \brief Constructor
   */
  


  public Transformation() throws InvalidIndexValueException
  {
        matrix = new Matrix(3,3);
        orientation = 1;
        pPosition = 0;
        colorPermutation = new Vector<Integer>();//TODO not sure about this line
        fileOrientation = 0;
  } // Constructor
  
  /*!
   * \brief Constructor
   *
   * \param initialMatrix the matrix to initialize the internal Matrix to
   */
  public Transformation(Matrix initialMatrix)
  {
     matrix = new Matrix(initialMatrix);
    orientation = 1;
    pPosition = 0;
    colorPermutation = new Vector<Integer>();//TODO not sure about this line
    fileOrientation = 0;

  } // Constructor
  public Transformation(Transformation t )
  {
    this.matrix = new Matrix(t.matrix);
    this.orientation = t.orientation;
    this.pPosition = t.pPosition;
    colorPermutation = new Vector<Integer>();
    for(int i =0; i < t.colorPermutation.size();i++){
         colorPermutation.add(t.colorPermutation.elementAt(i));
    }
    this.fileOrientation = t.fileOrientation;

  } // Constructor
  
  public Transformation assign(Transformation t)
  { 
      this.matrix = new Matrix(t.matrix);
      this.orientation = t.orientation;
      this.pPosition = t.pPosition;
      colorPermutation = new Vector<Integer>();
      for(int i =0; i < t.colorPermutation.size();i++){
         colorPermutation.setElementAt(t.colorPermutation.elementAt(i),i);
     }
     this.fileOrientation = t.fileOrientation;    
     return this;
  }
  
  public Transformation times( Transformation right) throws DimensionMismatchException, InvalidIndexValueException 
  {
    Transformation returnValue = new Transformation(this.matrix);
    //this.matrix.printElements();
    //TODO check the following line
   // returnValue.matrix.equals(returnValue.matrix.times(right.matrix));
    returnValue.matrix.timesEquals(right.matrix);
    returnValue.pPosition = right.pPosition;
    returnValue.orientation = this.orientation * right.orientation;
    for(int i=0; i < right.colorPermutation.size(); ++i)
    {
      returnValue.colorPermutation.add(this.colorPermutation.elementAt(right.colorPermutation.elementAt(i).intValue()- 1));
    }
    return returnValue;
  } // times
  
  
  public static void main( String [] args) throws InvalidIndexValueException, DimensionMismatchException{
      
      System.out.println("Testing Constructors");

      Transformation t1  = new Transformation();
      Matrix intialMatrix =  new Matrix(3,3,4);
      Matrix intialMatrix2 =  new Matrix(3,3,5);

      Transformation t2  = new Transformation(intialMatrix);
      Transformation t3  = new Transformation(intialMatrix2);
      Vector<Integer> v = new Vector<Integer>();
      for(int i =1 ;i < 5; i ++){
        v.add(i);
      }
      t2.colorPermutation = v;
      t3.colorPermutation = v;
      

      // TODO It fails the times function Check it again - Null pointer exception
      System.out.println("testing times Functions");
      Transformation t = (t2.times(t3));
      System.out.println(t.pPosition);
      System.out.println(t.orientation);
      System.out.println(t.fileOrientation);
      for(int i = 0; i <t.colorPermutation.size(); i++){
           System.out.println( t.colorPermutation.elementAt(i));
      }
      t.matrix.printElements();
      

      
      
      
  }
  
  
}



