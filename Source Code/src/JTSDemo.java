
import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.DecompositionSolver;
import org.apache.commons.math3.linear.LUDecomposition;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;

import com.vividsolutions.jts.algorithm.HCoordinate;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.LineSegment;
import com.vividsolutions.jts.geom.Triangle;

public class JTSDemo {

	public static void main(String args[]) {

		Coordinate c11 = new Coordinate(0, 0);
		Coordinate c12 = new Coordinate(0, 2);
		Coordinate c21 = new Coordinate(2, 0);
		Coordinate c22 = new Coordinate(2, 2);

		LineSegment ls = new LineSegment(c11, c12);
		LineSegment ls2 = new LineSegment(c21, c22);

	
		double slope_pb1 =0;
		double slope_pb2 =0;
		
		int count =0;

		while(slope_pb1 == slope_pb2)
		{
			double slope1 = (c12.y - c11.y) / (c12.x - c11.x);
			slope_pb1 = -1 / slope1;

			double slope2 = (c22.y - c21.y) / (c22.x - c21.x);
			slope_pb2 = -1 / slope2;
			
			System.out.println("Slope1 is:"+ slope_pb1+" Slope2 is: "+ slope_pb2);
			
			if(slope_pb1 == slope_pb2){
				
				if(count==0)
				{
					Coordinate temp = c12;
					c12 = c21;
					c21 = temp;
					
					ls = new LineSegment(c11, c12);
					ls2 = new LineSegment(c21, c22);
					
				}
				
				if(count==1)
				{
					Coordinate temp = c12;
					c12 = c22;
					c22 = temp;
					ls = new LineSegment(c11, c12);
					ls2 = new LineSegment(c21, c22);
				}
				count++;
			}
		}
		
		Coordinate midpb1 = ls.midPoint();
		Coordinate midpb2 = ls2.midPoint();

		double c1 = midpb1.y - midpb1.x*slope_pb1;

		double c2 = midpb2.y - midpb2.x*slope_pb2;
		
		double poi1 = (c2-c1)/(slope_pb1-slope_pb2);
		double poi2 = slope_pb1*(poi1) + c1;
		
		System.out.println(poi1+" "+poi2);
		
		
		RealMatrix coefficients =
			    new Array2DRowRealMatrix(new double[][] { {1, -1}, {1, -1}},
			                       false);
		DecompositionSolver solver = new LUDecomposition(coefficients).getSolver();
			
		RealVector constants = new ArrayRealVector(new double[] { 0, -1 }, false);
		RealVector solution = solver.solve(constants);
		
		System.out.println("New Center via LU is: " + solution);

	}
	
	public static void findCenter(){
		
	}
	

}
