
import java.util.Vector;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor. 
 */

/**
 *
 * @author maneeshavejendla
 */
public class Utility {
    
    
    ////////////////////////////////////////////////////////////////////////////////
    // verifyAndArrangePoints
    ////////////////////////////////////////////////////////////////////////////////
    /*!
    * \brief A utility function to create the motif
    *
    * \param points A vector containing MotifPoint objects
    *
    * \return A vector containing MotifPoint objects sorted by color and verified
    */
    
    static Vector<MotifPoint> verifyAndArrangePoints(Vector<MotifPoint> points) throws InvalidPointTypeException, InvalidIndexValueException
    {
      for( int i = 0; i < points.size(); ++i)
      {
        /* Comment out the print statements
        System.out.println("See what we got for point type");
        System.out.println(points.elementAt(i).pointTyp);
        System.out.println(PointTypeNum.getPointTypeFromInt(points.elementAt(i).pointTyp));
        System.out.println("See what we got for color type");
        System.out.println(points.elementAt(i).motifColor);
        System.out.println(Colorenum.getColorFromInt(points.elementAt(i).motifColor));*/
        
        switch(PointTypeNum.getPointTypeFromInt(points.elementAt(i).pointTyp))
        {
            case POINT_TYPE_MOVE_TO:
                break;
            case POINT_TYPE_DRAW_TO:
                break;
            case POINT_TYPE_CIRCLE:
          // Must be 2 in succession
 //TODO Check the following line is compsring int and int..It is comparing point type and point type
          if((i < 1 || PointTypeNum.getPointTypeFromInt(points.elementAt(i-1).pointTyp) != PointType.POINT_TYPE_CIRCLE) &&
             (i >= points.size() - 1 || PointTypeNum.getPointTypeFromInt(points.elementAt(i+1).pointTyp) != PointType.POINT_TYPE_CIRCLE))
          {
            throw new InvalidPointTypeException();
          }
          else
          {

            ++i;
          }
          break;
        case POINT_TYPE_START_FILLED_POLYGON:
          do
          {
            ++i;
            switch((PointTypeNum.getPointTypeFromInt(points.elementAt(i).pointTyp)))
            {
            case POINT_TYPE_CONTINUE_FILLED_POLYGON:
            case POINT_TYPE_END_FILLED_POLYGON:
              break;
            default:
              throw new InvalidPointTypeException();
            }
          } while(PointTypeNum.getPointTypeFromInt(points.elementAt(i).pointTyp) != PointType.POINT_TYPE_END_FILLED_POLYGON);
          break;
        case POINT_TYPE_HYPERLINE:
          // Must be 2 in succession
          if((i < 1 || PointTypeNum.getPointTypeFromInt(points.elementAt(i-1).pointTyp) != PointType.POINT_TYPE_HYPERLINE) &&
             (i >= points.size() - 1 ||
              PointTypeNum.getPointTypeFromInt(points.elementAt(i+1).pointTyp) != PointType.POINT_TYPE_HYPERLINE))
          {
            throw  new InvalidPointTypeException();
          }
          else
          {
            // Good
            ++i;
          }
          break;
        case POINT_TYPE_FILLED_CIRCLE:
          // Must be 2 in succession
          if((i < 1 || PointTypeNum.getPointTypeFromInt(points.elementAt(i-1).pointTyp) != PointType.POINT_TYPE_FILLED_CIRCLE) &&
             (i >= points.size() - 1 ||
              PointTypeNum.getPointTypeFromInt(points.elementAt(i+1).pointTyp) != PointType.POINT_TYPE_FILLED_CIRCLE))
          {
            throw new InvalidPointTypeException();
          }
          else
          {
            // Good
            ++i;
          }
          break;
        case POINT_TYPE_START_POLYLINE:
          do
          {
            ++i;
             switch((PointTypeNum.getPointTypeFromInt(points.elementAt(i).pointTyp)))
            {
            case POINT_TYPE_CONTINUE_POLYLINE:
            case POINT_TYPE_END_POLYLINE:
              break;
            default:
              throw new InvalidPointTypeException();
            }
          } while(PointTypeNum.getPointTypeFromInt(points.elementAt(i).pointTyp) != PointType.POINT_TYPE_END_POLYLINE);
          break;
        case POINT_TYPE_START_FILLED_PGON:
          do
          {
            ++i;
            switch((PointTypeNum.getPointTypeFromInt(points.elementAt(i).pointTyp)))
            {
            case POINT_TYPE_CONTINUE_FILLED_PGON:

            case POINT_TYPE_END_FILLED_PGON:
              break;
            default:
              throw new InvalidPointTypeException();
            }
          } while(PointTypeNum.getPointTypeFromInt(points.elementAt(i).pointTyp)!= PointType.POINT_TYPE_END_FILLED_PGON);
          break;
        case POINT_TYPE_EQUIDISTANT_CURVE:
            // Must be 3 in succession
   //TODO Check the following line is comprising int and int..It is comparing point type and point type
            if((i < 1 || PointTypeNum.getPointTypeFromInt(points.elementAt(i-1).pointTyp) != PointType.POINT_TYPE_EQUIDISTANT_CURVE) &&
               (i >= points.size() - 1 || PointTypeNum.getPointTypeFromInt(points.elementAt(i+1).pointTyp) != PointType.POINT_TYPE_EQUIDISTANT_CURVE) &&
               PointTypeNum.getPointTypeFromInt(points.elementAt(i+2).pointTyp) != PointType.POINT_TYPE_EQUIDISTANT_CURVE)
            {
              throw new InvalidPointTypeException();
            }
            else
            {

              ++i;
            }
            break;
            
        case POINT_TYPE_HYPERBOLIC_LINE:
            // Must be 2 in succession
            if((i < 1 || PointTypeNum.getPointTypeFromInt(points.elementAt(i-1).pointTyp) != PointType.POINT_TYPE_HYPERBOLIC_LINE) &&
               (i >= points.size() - 1 ||
                PointTypeNum.getPointTypeFromInt(points.elementAt(i+1).pointTyp) != PointType.POINT_TYPE_HYPERBOLIC_LINE))
            {
              throw new InvalidPointTypeException();
            }
            else
            {
              // Good
              ++i;
            }
            break;
        case POINT_TYPE_HYPERBOLIC_LINESEGMENT:
            // Must be 2 in succession
            if((i < 1 || PointTypeNum.getPointTypeFromInt(points.elementAt(i-1).pointTyp) != PointType.POINT_TYPE_HYPERBOLIC_LINESEGMENT) &&
               (i >= points.size() - 1 ||
                PointTypeNum.getPointTypeFromInt(points.elementAt(i+1).pointTyp) != PointType.POINT_TYPE_HYPERBOLIC_LINESEGMENT))
            {
              throw new InvalidPointTypeException();
            }
            else
            {
              // Good
              ++i;
            }
            break;
        case POINT_TYPE_HOROCYCLE:
        	break;
        case POINT_TYPE_HOROCYCLE_LINES:
        	break;
            
        default:
          throw new InvalidPointTypeException();
        }
      }
      return points;
    } // verifyAndArrangePoints
    
    
    
    
    ////////////////////////////////////////////////////////////////////////////////
    // stereofyPoint
    ////////////////////////////////////////////////////////////////////////////////
    
    /*!
    * \brief A utility function to translate from 2D point to 3D point
    *
    * \param x The x component of the point
    * \param y The y component of the point
    *
    * \return A VectorList with the point converted with element 1 as x,
    *         element 2 as y, element 3 as z
    */
    
    static VectorList stereofyPoint(double x, double y) throws InvalidIndexValueException
    {
      VectorList returnValue = new VectorList(3);
      double xSquared = x * x;
      double ySquared = y * y;
      double denominator = 1.0 - xSquared - ySquared;
      returnValue.setElement(1, (2.0 * x) / denominator);
      returnValue.setElement(2, (2.0 * y) / denominator);
      returnValue.setElement(3, (2.0 - denominator)  / denominator);
      return returnValue;
    } // stereofyPoint
    

    ////////////////////////////////////////////////////////////////////////////////
    // stereofyPoint
    ////////////////////////////////////////////////////////////////////////////////
    
    /*!
    * \brief A utility function to translate from 2D point to 3D point
    *
    * \param xy A VectorList of size 2 or more with x component in element 1
    *        and y component in element 2
    *
    * \return A VectorList with the point converted with element 1 as x,
    *         element 2 as y, element 3 as z
    * \throws InvalidIndexValueException if xy.getSize() < 2
    */
    
   static  public VectorList stereofyPoint(final VectorList xy)
      throws InvalidIndexValueException
    {
      if(xy.getSize() < 2)
      {
        throw new InvalidIndexValueException();
      }

      return stereofyPoint(xy.getElement(1),
                           xy.getElement(2));
    } // stereofyPoint

    
    ////////////////////////////////////////////////////////////////////////////////
    // destereofyPoint
    ////////////////////////////////////////////////////////////////////////////////
    /*!
    * \brief A utility function to translate from 3D point to 2D point
    *
    * \param x The x component of the point
    * \param y The y component of the point
    * \param z The z component of the point
    *
    * \return A VectorList with the point converted with element 1 as x and
    *         element 2 as y
    */
    
    VectorList destereofyPoint(double x, double y, double z) throws InvalidIndexValueException
    {
      VectorList returnValue = new VectorList(2);
      double denominator = 1.0 + z;
      returnValue.setElement(1, x);
      returnValue.setElement(2, y);
      returnValue.applyScalar(1.0 / denominator);
      return returnValue;
    } // destereofyPoint

    
    ////////////////////////////////////////////////////////////////////////////////
    // destereofyPoint
    ////////////////////////////////////////////////////////////////////////////////
    /*!
    * \brief A utility function to translate from 3D point to 2D point
    *
    * \param xyz A VectorList of size 3 or more with x component in
    *        element 1, y component in element 2, and z component in element 3
    *
    * \return A VectorList with the point converted with element 1 as x and
    *         element 2 as y
    * \throws InvalidIndexValueException if xyz.getSize() < 3
    */
    
    VectorList destereofyPoint(final VectorList xyz)
      throws InvalidIndexValueException
    {
      if(xyz.getSize() < 3)
      {
        throw new InvalidIndexValueException();
      }

      return destereofyPoint(xyz.getElement(1),
                             xyz.getElement(2),
                             xyz.getElement(3));
    } // destereofyPoint
    
    
    ////////////////////////////////////////////////////////////////////////////////
    // translatePoincareToKlein
    ////////////////////////////////////////////////////////////////////////////////
     /*!
    * \brief A utility function to translate from Poincare model to Klein model
    * \param uv A 2 element VectorList to convert with u in element 1 and
    *           v in element 2
    *
    * \return A VectorList with the point converted where
    *         element 1 is x, element 2 is y
    *
    * \throws InvalidIndexValueException if uv.getSize() < 2
    */
    
    static VectorList translatePoincareToKlein(final VectorList uv)
      throws InvalidIndexValueException
    {
      if(uv.getSize() < 2)
      {
        throw new InvalidIndexValueException();
      }
      return translatePoincareToKlein(uv.getElement(1),
                                      uv.getElement(2));
    } // translatePoincareToKlein
    
    

    ////////////////////////////////////////////////////////////////////////////////
    // translatePoincareToKlein
    ////////////////////////////////////////////////////////////////////////////////
    /*!
    * \brief A utility function to translate from Poincare model to Klein model
    * \param u The u value to use
    * \param v The v value to use
    *
    * \return A VectorList with the point converted where
    *         element 1 is x, element 2 is y
    */

    static VectorList translatePoincareToKlein(double u, double v) throws InvalidIndexValueException
    {
      VectorList  returnValue = new VectorList(2);
      double denominator = 1 + u * u + v * v;
      returnValue.setElement(1, 2 * u / denominator);
      returnValue.setElement(2, 2 * v / denominator);
      return returnValue;
    } // translatePoincareToKlein
    
    
    ////////////////////////////////////////////////////////////////////////////////
    // translateKleinToPoincare
    ////////////////////////////////////////////////////////////////////////////////
     /*!
    * \brief A utility function to translate from Klein model to Poincare model
    * \param xy A 2 element VectorList to convert with x in element 1 and
    *           y in element 2
    *
    * \return A VectorList with the point converted where
    *         element 1 is u, element 2 is v
    *
    * \throws InvalidIndexValueException if xy.getSize() < 2
    */
    
    static VectorList translateKleinToPoincare(final VectorList xy)
      throws InvalidIndexValueException
    {
      if(xy.getSize() < 2)
      {
        throw new InvalidIndexValueException();
      }
      return translateKleinToPoincare(xy.getElement(1),
                                      xy.getElement(2));
    } // translateKleinToPoincare

    ////////////////////////////////////////////////////////////////////////////////
    // translateKleinToPoincare
    ////////////////////////////////////////////////////////////////////////////////
    /*!
    * \brief A utility function to translate from Klein model to Poincare model
    * \param x The x value to use
    * \param y The y value to use
    *
    * \return A VectorList with the point converted where
    *         element 1 is u, element 2 is v
    */
    
    static VectorList translateKleinToPoincare(double x, double y) throws InvalidIndexValueException
    {
      VectorList returnValue =  new VectorList(2);
      double denominator = 1 + Math.sqrt(1 - x * x - y * y);
      returnValue.setElement(1, x / denominator);
      returnValue.setElement(2, y / denominator);
      return returnValue;
    } // translateKleinToPoincare

     public static void main(String args[]) throws InvalidIndexValueException, DimensionMismatchException, InvalidPointTypeException{
      
      Utility u1 = new Utility();
      VectorList v1 = new VectorList(2,0.1);
      VectorList v2 = new VectorList(2,3.0);
      VectorList v10 = new VectorList(3,3.0);
      VectorList v0 = u1.destereofyPoint(v10);
      System.out.println("Printing the elements after destereo of y point");          
      v0.printElements();
      VectorList v3 = u1.destereofyPoint(1.0,2.0,3.0);
      System.out.println("Printing the elements after destereo of y point");          
      v3.printElements();
      VectorList v4 = u1.translatePoincareToKlein(v2);
      System.out.println("Printing the elements after translating from Poincare to Klein");          
      v4.printElements();
      VectorList v5 = u1.translatePoincareToKlein(1.0, 2.0);
      System.out.println("Printing the elements after translating from Poincare to Klein");          
      v5.printElements();
      VectorList v6 = u1.translateKleinToPoincare(v1);
      System.out.println("Printing the elements after translating from Klein to Poincare");          
      v6.printElements();
      VectorList v7 = u1.translateKleinToPoincare(0.1, 0.2);
      System.out.println("Printing the elements after translating from Klein to Poincare");          
      v7.printElements();
      VectorList v8 = u1.stereofyPoint(1.0, 2.0);
      System.out.println("Printing the elements after stereo of y point");          
      v8.printElements();
      VectorList v9 = u1.stereofyPoint(v10);
      System.out.println("Printing the elements after stereo of y point");          
      v9.printElements();
      MotifPoint m1 = new MotifPoint(1.0,1.0,1.0,0,1);
      MotifPoint m2 = new MotifPoint(2.0,2.0,2.0,10,2);
      MotifPoint m3 = new MotifPoint(3.0,3.0,3.0,7,1);
      Vector<MotifPoint> v = new Vector<>();
      v.add(m1);
      v.add(m2);
      v.add(m3);
      System.out.println("the size of the vector of motif points is");
      System.out.println(v.size());
      Vector<MotifPoint> v11 = u1.verifyAndArrangePoints(v);
      System.out.println("The motif points after verifying and arranging the motif points are:");
      for(int i =0; i<v11.size(); i++){ //TODO Run and see.. Careful while validating the pointtype
          MotifPoint m = v11.elementAt(i);
          m.printElements();
          PointTypeNum p = new PointTypeNum();
          System.out.println(p.pointTypeToString(p.getPointTypeFromInt(m.pointTyp)));
          System.out.println("********");
          
      }
        
            
   
      
  }


}
