
import java.awt.BorderLayout;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.util.Vector;
import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import static javax.swing.WindowConstants.DISPOSE_ON_CLOSE;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author maneeshavejendla
 */
public class EditPointDialog extends JDialog{
      
  int minPoints;
  int maxPoints;
  int maxColor;
  JButton addPointButton;
  JButton removePointButton;
  Vector<MotifPoint> points;
  ObjectType type;

  JPanel centralGrid;
  JLabel typeLabel;
  JComboBox colors;

  Vector<JSpinner> pointXs;
  Vector<JSpinner> pointYs;

  JPanel pointXsGrid;
  JPanel pointYsGrid;
  
  
    public  EditPointDialog(int color, ObjectType obj, int max, int min) throws InvalidIndexValueException{
        super();
        initUI(color, obj, max, min);
        showEvent(); //TODO uncomment
    }
        
    public final void initUI(int color, ObjectType obj, int max, int min) { 
        
        setLayout(new BoxLayout(getContentPane(), BoxLayout.PAGE_AXIS));
        
        maxColor = Colorenum.getIntFromDefinedColor(DefinedColor.COLOR_BROWN);
        maxPoints = 0;
        minPoints = 1;
        colors = new JComboBox();
        
        JPanel typePanel = new JPanel();
        typePanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Type"));
        typeLabel = new JLabel();
        typePanel.add(typeLabel);
        
        JPanel pointsPanel = new JPanel( new GridLayout(2,0));
        pointsPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Points"));    
        pointXsGrid = new JPanel();
        pointXsGrid.setBorder(javax.swing.BorderFactory.createTitledBorder("x"));   
        pointYsGrid = new JPanel();
        pointYsGrid.setBorder(javax.swing.BorderFactory.createTitledBorder("y"));
        pointsPanel.add(pointXsGrid);
        pointsPanel.add(pointYsGrid);
        
        addPointButton = new javax.swing.JButton();
        addPointButton.setText("Add point");
        //addPointButton.setEnabled(false);
        addPointButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addPointPushed();
            }
        });
        
        
        removePointButton = new javax.swing.JButton();
        removePointButton.setText("Remove Point");
        removePointButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                removePointPushed();
            }
        });

        
        JButton acceptButton = new javax.swing.JButton();
        acceptButton.setText("Add/Edit Object");
        acceptButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                accept();
            }
        });
        acceptButton.setAlignmentX(0.5f);
        JButton cancelButton = new javax.swing.JButton();
        cancelButton.setText("Cancel");
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                reject();
            }
        });
        cancelButton.setAlignmentX(0.5f);
        
        
        centralGrid = new JPanel(new BorderLayout());
        JPanel firstLayer =  new JPanel(new FlowLayout());
        firstLayer.add(typePanel);
        firstLayer.add(colors);
        centralGrid.add(firstLayer, BorderLayout.NORTH);
        centralGrid.add(pointsPanel, BorderLayout.CENTER);
        JPanel thirdLayer =  new JPanel(new FlowLayout());
        thirdLayer.add(addPointButton);
        thirdLayer.add(removePointButton);
        thirdLayer.add(acceptButton);
        thirdLayer.add(cancelButton);
        centralGrid.add(thirdLayer, BorderLayout.SOUTH);
        
        add(centralGrid);


        setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setLocationRelativeTo(null);
        setSize(400, 400);
        
        points = new Vector<MotifPoint>();
        pointXs = new Vector<JSpinner>();
        pointYs = new Vector<JSpinner>();
        
        setMaximumColor(color);
        setObjectType(obj);
        setMaximumPoints(max);
        setMinimumPoints(min);
        
        
    }
    
     ////////////////////////////////////////////////////////////////////////////////
     // accept
     ////////////////////////////////////////////////////////////////////////////////
     /*!
     * \brief Overrides the standard accept slot
     */
     
     public void accept(){
         boolean goodToGo = true;
        for( int i = 0; i < pointXs.size(); ++i)
        {
          double x = (double) pointXs.elementAt(i).getValue();//TODO get the int value
          double y = (double) pointYs.elementAt(i).getValue();
          if(x * x + y * y > 1.0)
          {
              int number = i+1;
              JOptionPane.showMessageDialog(null,"Point "+number+ " must be within the unit circle." );
            goodToGo = false;
          }
        }
        if(goodToGo)
        {
          points.clear();
          MotifPoint temp = new MotifPoint();
          temp.w = 0;
          temp.motifColor = colors.getSelectedIndex() + 1;
          for( int i = 0; i < pointXs.size(); ++i)
          {
            temp.x = (double) pointXs.elementAt(i).getValue();
            temp.y = (double) pointYs.elementAt(i).getValue();
            switch(type)
            {
            case OBJECT_TYPE_MOVE_TO:
              temp.pointTyp = PointTypeNum.getIntFromPointType(PointType.POINT_TYPE_MOVE_TO);
              break;
            case OBJECT_TYPE_DRAW_TO:
              temp.pointTyp = PointTypeNum.getIntFromPointType(PointType.POINT_TYPE_DRAW_TO);
              break;
            case OBJECT_TYPE_CIRCLE:
              temp.pointTyp = PointTypeNum.getIntFromPointType(PointType.POINT_TYPE_CIRCLE);
              break;
            case OBJECT_TYPE_FILLED_POLYGON:
              if(0 == i)
              {
                temp.pointTyp = PointTypeNum.getIntFromPointType(PointType.POINT_TYPE_START_FILLED_POLYGON);
              }
              else if(pointXs.size() - 1 == i)
              {
                temp.pointTyp = PointTypeNum.getIntFromPointType(PointType.POINT_TYPE_END_FILLED_POLYGON);
              }
              else
              {
                temp.pointTyp = PointTypeNum.getIntFromPointType(PointType.POINT_TYPE_CONTINUE_FILLED_POLYGON);
              }
              break;
            case OBJECT_TYPE_HYPERLINE:
              temp.pointTyp = PointTypeNum.getIntFromPointType(PointType.POINT_TYPE_HYPERLINE);
              break;
            case OBJECT_TYPE_FILLED_CIRCLE:
              temp.pointTyp = PointTypeNum.getIntFromPointType(PointType.POINT_TYPE_FILLED_CIRCLE);
              break;
            case OBJECT_TYPE_POLYLINE:
              if(0 == i)
              {
                temp.pointTyp = PointTypeNum.getIntFromPointType(PointType.POINT_TYPE_START_POLYLINE);
              }
              else if(pointXs.size() - 1 == i)
              {
                temp.pointTyp = PointTypeNum.getIntFromPointType(PointType.POINT_TYPE_END_POLYLINE);
              }
              else
              {
                temp.pointTyp = PointTypeNum.getIntFromPointType(PointType.POINT_TYPE_CONTINUE_POLYLINE);
              }
              break;
            case OBJECT_TYPE_FILLED_PGON:
              if(0 == i)
              {
                temp.pointTyp = PointTypeNum.getIntFromPointType(PointType.POINT_TYPE_START_FILLED_PGON);
              }
              else if(pointXs.size() - 1 == i)
              {
                temp.pointTyp = PointTypeNum.getIntFromPointType(PointType.POINT_TYPE_END_FILLED_PGON);
              }
              else
              {
                temp.pointTyp = PointTypeNum.getIntFromPointType(PointType.POINT_TYPE_CONTINUE_FILLED_PGON);
              }
              break;
            }
            MotifPoint m = new MotifPoint(temp);
            points.add(m);
          }
          cleanup();
          this.dispose();
        }
         
     }
     ////////////////////////////////////////////////////////////////////////////////
     // showEvent
     ////////////////////////////////////////////////////////////////////////////////
     /*!
     * \brief Overrides the standard showEvent slot
     *
     * \param s Variable for override
     */
     public void showEvent() throws InvalidIndexValueException{  
         typeLabel.setText(ObjectTypeEnum.objectTypeToString(type));
         colors.removeAll();//TODO check again
         Vector<String> itemStrings =  new Vector<String>();
        for( int i = 1; i <= maxColor; ++i)
        {
           //System.out.println(Colorenum.colorToQString(Colorenum.getColorFromInt(i)));
          itemStrings.add(Colorenum.colorToQString(Colorenum.getColorFromInt(i)));//TODO Write this line again
        }

        
        final DefaultComboBoxModel model = new DefaultComboBoxModel(itemStrings);
         colors.setModel(model);
         if(points.size() > 0)
        {
          colors.setSelectedIndex(points.elementAt(0).motifColor - 1);//TODO Write this line again
        }

        for( int i = 0; i < minPoints; ++i)
        {
          if(i < points.size())
          {
            addPoint(points.elementAt(i).x, points.elementAt(i).y);
          }
          else
          {
            addPoint(0.0, 0.0);
          }
        }
        removePointButton.setEnabled(false);
        if(0 == maxPoints || pointXs.size() < maxPoints)
        {
          addPointButton.setEnabled(true);
        }
        else
        {
          addPointButton.setEnabled(false);
        }
         
     }
     
     
       /*!
   * \brief A utility function to remove the last point in the list
   */
     ////////////////////////////////////////////////////////////////////////////////
    // removeLastPoint
    ////////////////////////////////////////////////////////////////////////////////
      private void removeLastPoint(){
          System.out.println("In remove last point");
          
          pointXsGrid.removeAll();
          pointYsGrid.removeAll();
          this.repaint();
         System.out.println("All the components of the x and y panel are removed by now");

          pointXs.removeElementAt(pointXs.size()-1);
          pointYs.removeElementAt(pointYs.size()-1);
          
          for(int i = 0; i<pointXs.size();i++){
            pointXsGrid.add(pointXs.elementAt(i));
          }
          for(int i = 0; i<pointYs.size();i++){
            pointYsGrid.add(pointYs.elementAt(i));
          }
          

      }
      
     
////////////////////////////////////////////////////////////////////////////////
// addPoint
//////////////////////////////////////////////////////////////////////////////// 
    /*!
   * \brief A utility function to add a point to the list with coordinate (x, y)
   *
   * \param x The x coordinate for the point
   * \param y The y coordinate for the point
   */
  private void addPoint(double x, double y){
      //System.out.println(x+" "+y);
      JSpinner tempX =  new JSpinner();
       tempX = constructSpinBox(x, tempX); //TODO check the function constructSpinBox
       //System.out.println(tempX);
       pointXs.add(tempX);
       pointXsGrid.add(tempX);

       JSpinner tempY = new JSpinner();
       tempY = constructSpinBox(y, tempY);

       pointYs.add(tempY);
       pointYsGrid.add(tempY);
      
  }

     ////////////////////////////////////////////////////////////////////////////////
     // addPointPushed
     ////////////////////////////////////////////////////////////////////////////////
     /*!
     * \brief A function called when the Add Point Button is clicked
     */
    
     private void addPointPushed(){
         
           if(0 == maxPoints || pointXs.size() < maxPoints)
            {
              addPoint(0.0, 0.0);
              removePointButton.setEnabled(true);
            }
            if(maxPoints > 0 && pointXs.size() >= maxPoints)
            {
              addPointButton.setEnabled(false);
            }
         
     }

     ////////////////////////////////////////////////////////////////////////////////
     // removePointPushed
     ////////////////////////////////////////////////////////////////////////////////
     /*!
     * \brief A function called when the Remove Point Button is clicked
     */
     private void removePointPushed(){
           if(pointXs.size() > minPoints)
            {
              removeLastPoint();
              if(pointXs.size() <= minPoints)
              {
                removePointButton.setEnabled(false);
              }
              if(0 == maxPoints || pointXs.size() < maxPoints)
              {
                addPointButton.setEnabled(true);
              }
              else
              {
                addPointButton.setEnabled(false);
              }
            }
         
     }


////////////////////////////////////////////////////////////////////////////////
// cleanup
////////////////////////////////////////////////////////////////////////////////
  /*!
   * \brief A utility function to perform various cleanup operations
   */
  private void cleanup(){
        int size = pointXs.size();
        for( int i = 0; i < size; ++i)
        {
          removeLastPoint();
        }
      
  }
  
////////////////////////////////////////////////////////////////////////////////
// constructSpinBox
////////////////////////////////////////////////////////////////////////////////

  /*!
   * \brief A utility function to construct a spin box
   *
   * \param value The current value to set the spin box to
   */
  private JSpinner constructSpinBox(double value, JSpinner returnValue){ //TODO Chcek this function again
        Double layerValue = new Double(value);
        Double layerMin = new Double(-1.0);
        Double layerMax = new Double(1);
        Double layerStep = new Double(0.01);    
        //returnValue->setDecimals(6); //TODO check this line
        SpinnerNumberModel model1 = new SpinnerNumberModel(layerValue, layerMin, layerMax, layerStep);
        returnValue.setModel(model1);
        JComponent field = ((JSpinner.DefaultEditor) returnValue.getEditor());
        Dimension prefSize = field.getPreferredSize();
        prefSize = new Dimension(50, prefSize.height);
        field.setPreferredSize(prefSize);
        return returnValue;
      
  }

    
   ////////////////////////////////////////////////////////////////////////////////
   // getPoints
   ////////////////////////////////////////////////////////////////////////////////
   /*!
   * \brief Returns the points generated after the dialog has been accepted
   *
   * \return a vector of points to add/edit
   */
    
    public Vector<MotifPoint> getPoints() {
        return points;
         
    }
    
   ////////////////////////////////////////////////////////////////////////////////
   // setMaximumColor
   ////////////////////////////////////////////////////////////////////////////////
   /*!
   * \brief Sets the maximum color number to use
   *
   * \param maxColor The maximum color number
   */
    
    public void setMaximumColor( int maxColor){
      if(maxColor > Colorenum.getIntFromDefinedColor(DefinedColor.COLOR_GREY) && maxColor <= Colorenum.getIntFromDefinedColor(DefinedColor.COLOR_BROWN))
      {
        this.maxColor = maxColor;
      }
        
    }
    
    ////////////////////////////////////////////////////////////////////////////////
    // setMaximumPoints
    ////////////////////////////////////////////////////////////////////////////////
    /*!
    * \brief Sets the maximum number of points to use
    *
    * \param maxPoints The maximum number of points
    */
    
    public void setMaximumPoints(int maxPoints){
        this.maxPoints = maxPoints;
    }
    
    
    ////////////////////////////////////////////////////////////////////////////////
    // setMinimumPoints
    ////////////////////////////////////////////////////////////////////////////////
    /*!
    * \brief Sets the minimum number of points to use
    *
    * \param minPoints The minimum number of points
    */
    
     public void setMinimumPoints(int minPoints){
         if(minPoints > 0)
        {
          this.minPoints = minPoints;
        }
    
    }
     ////////////////////////////////////////////////////////////////////////////////
     // setObjectType
     ////////////////////////////////////////////////////////////////////////////////
     /*!
     * \brief Sets the object type for the object being added/edited
     *
     * \param type The object type
     */
     
     public void setObjectType(ObjectType type){
           this.type = type;
     }
     
     ////////////////////////////////////////////////////////////////////////////////
     // setPoints
     ////////////////////////////////////////////////////////////////////////////////
     /*!
     * \brief Sets the points to use before displaying the dialog
     *
     * \param points The points to use
     */ 
     
     public void setPoints(Vector<MotifPoint> points){
         this.points = points;
     }
  

     ////////////////////////////////////////////////////////////////////////////////
     // close
     ////////////////////////////////////////////////////////////////////////////////
     /*!
      * \brief Overrides the standar close slot
      */
     
      public void close(){
          cleanup();
         this.dispose();
      }

     ////////////////////////////////////////////////////////////////////////////////
     // reject
     ////////////////////////////////////////////////////////////////////////////////
     /*!
     * \brief Overrides the standard reject slot
     */
      
     public void reject(){
         cleanup();
       this.dispose();
         
     }



}
