import java.util.Vector;

import javax.swing.JOptionPane;

public class EditPoints {
	public Vector<MotifPoint> accept(Vector<MotifPoint> points, ObjectType type){
	//	points.remove(0);
		boolean goodToGo = true;
		for( int i = 0; i < points.size(); ++i)
		{
			double x = (double) points.get(i).x;//TODO get the int value
			double y = (double) points.get(i).y;
			if(x * x + y * y > 1.0)
			{
				int number = i+1;
				JOptionPane.showMessageDialog(null,"Point "+number+ " must be within the unit circle." );
				goodToGo = false;
			}
		}
		if(goodToGo)
		{
		//	points.clear();
			//         MotifPoint temp = new MotifPoint();
			//         temp.w = 0;
			//temp.motifColor = colors.getSelectedIndex() + 1;
			//temp.motifColor = 3; //Should be taken from user
			for( int i = 0; i < points.size(); ++i)
			{
				MotifPoint temp = points.get(i);
				switch(type)
				{
				case OBJECT_TYPE_MOVE_TO:
					temp.pointTyp = PointTypeNum.getIntFromPointType(PointType.POINT_TYPE_MOVE_TO);
					break;
				case OBJECT_TYPE_DRAW_TO:
					temp.pointTyp = PointTypeNum.getIntFromPointType(PointType.POINT_TYPE_DRAW_TO);
					break;
				case OBJECT_TYPE_CIRCLE:
					temp.pointTyp = PointTypeNum.getIntFromPointType(PointType.POINT_TYPE_CIRCLE);
					break;
				case OBJECT_TYPE_FILLED_POLYGON:
					if(0 == i)
					{
						temp.pointTyp = PointTypeNum.getIntFromPointType(PointType.POINT_TYPE_START_FILLED_POLYGON);
					}
					else if(points.size() - 1 == i)
					{
						temp.pointTyp = PointTypeNum.getIntFromPointType(PointType.POINT_TYPE_END_FILLED_POLYGON);
					}
					else
					{
						temp.pointTyp = PointTypeNum.getIntFromPointType(PointType.POINT_TYPE_CONTINUE_FILLED_POLYGON);
					}
					break;
				case OBJECT_TYPE_HYPERLINE:
					temp.pointTyp = PointTypeNum.getIntFromPointType(PointType.POINT_TYPE_HYPERLINE);
					break;
				case OBJECT_TYPE_FILLED_CIRCLE:
					temp.pointTyp = PointTypeNum.getIntFromPointType(PointType.POINT_TYPE_FILLED_CIRCLE);
					break;
				case OBJECT_TYPE_POLYLINE:
					if(0 == i)
					{
						temp.pointTyp = PointTypeNum.getIntFromPointType(PointType.POINT_TYPE_START_POLYLINE);
					}
					else if(points.size() - 1 == i)
					{
						temp.pointTyp = PointTypeNum.getIntFromPointType(PointType.POINT_TYPE_END_POLYLINE);
					}
					else
					{
						temp.pointTyp = PointTypeNum.getIntFromPointType(PointType.POINT_TYPE_CONTINUE_POLYLINE);
					}
					break;
				case OBJECT_TYPE_FILLED_PGON:
					if(0 == i)
					{
						temp.pointTyp = PointTypeNum.getIntFromPointType(PointType.POINT_TYPE_START_FILLED_PGON);
					}
					else if(points.size() - 1 == i)
					{
						temp.pointTyp = PointTypeNum.getIntFromPointType(PointType.POINT_TYPE_END_FILLED_PGON);
					}
					else
					{
						temp.pointTyp = PointTypeNum.getIntFromPointType(PointType.POINT_TYPE_CONTINUE_FILLED_PGON);
					}
					break;
				}
			}
		}
		return points;
	}
}