/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author maneeshavejendla
 */

 enum PointType
  {
    POINT_TYPE_MOVE_TO (1),
    POINT_TYPE_DRAW_TO(2),
    POINT_TYPE_CIRCLE(3),
    POINT_TYPE_START_FILLED_POLYGON(4),
    POINT_TYPE_CONTINUE_FILLED_POLYGON(5),
    POINT_TYPE_END_FILLED_POLYGON(6),
    POINT_TYPE_HYPERLINE(7),
    POINT_TYPE_FILLED_CIRCLE(8),
    POINT_TYPE_START_POLYLINE(9),
    POINT_TYPE_CONTINUE_POLYLINE(10),
    POINT_TYPE_END_POLYLINE(11),
    POINT_TYPE_START_FILLED_PGON(12),
    POINT_TYPE_CONTINUE_FILLED_PGON(13),
    POINT_TYPE_END_FILLED_PGON(14),
    POINT_TYPE_EQUIDISTANT_CURVE(20),
    POINT_TYPE_HOROCYCLE(21),
    POINT_TYPE_HOROCYCLE_LINES(22),
    POINT_TYPE_HYPERBOLIC_LINESEGMENT(23),
    POINT_TYPE_HYPERBOLIC_LINE(24);
    
    private int enumVal;
    
    PointType(int enumVal ){
        this.enumVal = enumVal;
    }
    
    int getPointTypeEnumVal(){
        return enumVal;
    }
    
   
 }

public class PointTypeNum{

          
   // PointType
  PointType pointtype;
    
  private int pointNum;
  
  PointTypeNum(){
    
}
  
  PointTypeNum(PointType pointtype, int s){
       this.pointtype = pointtype;
       pointNum = s;
   }
   
   PointType getPointTypeEnum(){
       return pointtype;
   }
  
    public int getCompanyLetter() {
        return pointNum;
    }

  
////////////////////////////////////////////////////////////////////////////////
// pointTypeToString
////////////////////////////////////////////////////////////////////////////////
static String pointTypeToString(PointType type)
{
  String pointTypeName = null;
  switch(type)
  {
  case POINT_TYPE_MOVE_TO:
    pointTypeName = "Move To";
    break;
  case POINT_TYPE_DRAW_TO:
    pointTypeName = "Draw To";
    break;
  case POINT_TYPE_CIRCLE:
    pointTypeName = "Circle";
    break;
  case POINT_TYPE_START_FILLED_POLYGON:
    pointTypeName = "Filled Polygon Start";
    break;
  case POINT_TYPE_CONTINUE_FILLED_POLYGON:
    pointTypeName = "Filled Polygon Continue";
    break;
  case POINT_TYPE_END_FILLED_POLYGON:
    pointTypeName = "Filled Polygon End";
    break;
  case POINT_TYPE_HYPERLINE:
    pointTypeName = "Hyperline";
    break;
  case POINT_TYPE_FILLED_CIRCLE:
    pointTypeName = "Filled Circle";
    break;
  case POINT_TYPE_START_POLYLINE:
    pointTypeName = "Polyline Start";
    break;
  case POINT_TYPE_CONTINUE_POLYLINE:
    pointTypeName =  "Polyline Continue";
    break;
  case POINT_TYPE_END_POLYLINE:
    pointTypeName = "Polyline End";
    break;
  case POINT_TYPE_START_FILLED_PGON:
    pointTypeName = "Filled Pgon Start";
    break;
  case POINT_TYPE_CONTINUE_FILLED_PGON:
    pointTypeName = "Filled Pgon Continue";
    break;
  case POINT_TYPE_END_FILLED_PGON:
    pointTypeName = "Filled Pgon End";
    break;
  case POINT_TYPE_HYPERBOLIC_LINESEGMENT:
    pointTypeName = "Hyperbolic Line Segment";
    break;
  case POINT_TYPE_HYPERBOLIC_LINE:
    pointTypeName = "Hyperbolic Line";
    break;
  case POINT_TYPE_EQUIDISTANT_CURVE:
    pointTypeName = "Equidistant Curve";
    break;
  case POINT_TYPE_HOROCYCLE:
    pointTypeName = "Horocycle";
    break;
  case POINT_TYPE_HOROCYCLE_LINES:
    pointTypeName = "Horocycle With Lines";
    break;
  default:
    pointTypeName = "Unknown Type";
    break;
  }
  return pointTypeName;
} // pointTypeToString


////////////////////////////////////////////////////////////////////////////////
// getIntFromPointType
////////////////////////////////////////////////////////////////////////////////
static final int getIntFromPointType(PointType type)
{
  int pointTypeName = -1;
  switch(type)
  {
  case POINT_TYPE_MOVE_TO:
    pointTypeName = 1;
    break;
  case POINT_TYPE_DRAW_TO:
    pointTypeName = 2;
    break;
  case POINT_TYPE_CIRCLE:
    pointTypeName = 3;
    break;
  case POINT_TYPE_START_FILLED_POLYGON:
    pointTypeName = 4;
    break;
  case POINT_TYPE_CONTINUE_FILLED_POLYGON:
    pointTypeName = 5;
    break;
  case POINT_TYPE_END_FILLED_POLYGON:
    pointTypeName = 6;
    break;
  case POINT_TYPE_HYPERLINE:
    pointTypeName = 7;
    break;
  case POINT_TYPE_FILLED_CIRCLE:
    pointTypeName = 8;
    break;
  case POINT_TYPE_START_POLYLINE:
    pointTypeName = 9;
    break;
  case POINT_TYPE_CONTINUE_POLYLINE:
    pointTypeName =  10;
    break;
  case POINT_TYPE_END_POLYLINE:
    pointTypeName = 11;
    break;
  case POINT_TYPE_START_FILLED_PGON:
    pointTypeName = 12;
    break;
  case POINT_TYPE_CONTINUE_FILLED_PGON:
    pointTypeName = 13;
    break;
  case POINT_TYPE_END_FILLED_PGON:
    pointTypeName = 14;
    break;
  case POINT_TYPE_EQUIDISTANT_CURVE:
    pointTypeName = 20;
    break;
  case POINT_TYPE_HOROCYCLE:
    pointTypeName = 21;
    break;
  case POINT_TYPE_HOROCYCLE_LINES:
    pointTypeName = 22;
    break;
  case POINT_TYPE_HYPERBOLIC_LINESEGMENT:
    pointTypeName = 23;
    break;
  case POINT_TYPE_HYPERBOLIC_LINE:
    pointTypeName = 24;
    break;
  default:
    pointTypeName = -1;
    break;
  }
  return pointTypeName;
} // pointTypeToString

////////////////////////////////////////////////////////////////////////////////
// getPointTypeFromInt
////////////////////////////////////////////////////////////////////////////////
static PointType getPointTypeFromInt(int pointType)
    throws InvalidIndexValueException
{
  switch(pointType)
  {

  case 1:
    return  PointType.POINT_TYPE_MOVE_TO; 
  case 2:
    return  PointType.POINT_TYPE_DRAW_TO; 
  case 3:
    return  PointType.POINT_TYPE_CIRCLE;      
  case 4:
    return  PointType.POINT_TYPE_START_FILLED_POLYGON; 
  case 5:
    return  PointType.POINT_TYPE_CONTINUE_FILLED_POLYGON; 
  case 6:
    return  PointType.POINT_TYPE_END_FILLED_POLYGON; 
  case 7:
    return  PointType.POINT_TYPE_HYPERLINE; 
  case 8:
    return  PointType.POINT_TYPE_FILLED_CIRCLE; 
  case 9:
    return  PointType.POINT_TYPE_START_POLYLINE; 
  case 10:
    return  PointType.POINT_TYPE_CONTINUE_POLYLINE; 
  case 11:
    return  PointType.POINT_TYPE_END_POLYLINE;
  case 12:
    return  PointType.POINT_TYPE_START_FILLED_PGON;
  case 13:
    return  PointType.POINT_TYPE_CONTINUE_FILLED_PGON;
  case 14:
    return  PointType.POINT_TYPE_END_FILLED_PGON;
  case 20:
	return  PointType.POINT_TYPE_EQUIDISTANT_CURVE;
  case 21:
	return  PointType.POINT_TYPE_HOROCYCLE;
  case 22:
	return  PointType.POINT_TYPE_HOROCYCLE_LINES;
  case 23:
	return  PointType.POINT_TYPE_HYPERBOLIC_LINESEGMENT;
  case 24:
	return  PointType.POINT_TYPE_HYPERBOLIC_LINE;
  default:
    throw new InvalidIndexValueException();
  }
} // getPointTypeFromInt

public static void main(String[] args) throws InvalidIndexValueException{
    System.out.println("Testing the PointTypeEnum Class");
    PointTypeNum p = new PointTypeNum();
    String resultString1 = p.pointTypeToString(PointType.POINT_TYPE_CIRCLE);
    String resultString2 = p.pointTypeToString(PointType.POINT_TYPE_CONTINUE_FILLED_PGON);
    String resultString3 = p.pointTypeToString(PointType.POINT_TYPE_CONTINUE_POLYLINE);
    String resultString4 = p.pointTypeToString(PointType.POINT_TYPE_START_FILLED_PGON);
    String resultString5 = p.pointTypeToString(getPointTypeFromInt(1));

    System.out.println("The converted strings are ");
    System.out.println(resultString1);
    System.out.println(resultString2);
    System.out.println(resultString3);
    System.out.println(resultString4);
    System.out.println(resultString5);

    PointTypeNum.getPointTypeFromInt(1);
    PointTypeNum.getPointTypeFromInt(2);




    
}

  
  
  
}
