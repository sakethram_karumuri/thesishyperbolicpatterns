
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.Arc2D;
import java.awt.geom.Ellipse2D;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Vector;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author maneeshavejendla
 */
class MyDrawingPanel extends JPanel implements MouseListener{
	
	Vector<GraphicsItem> objectsList;
    double scale;
    int click_x, click_y;
    double poincare_x, poincare_y;
    ArrayList<MotifPoint> poincare_xy = new ArrayList<MotifPoint>();
    private final int max_x = 800;
    private final int max_y = 800;
    int currentObjectMaxPoints = 0;
    
    MainWindow main_win;
    
    public MyDrawingPanel (MainWindow obj)  {
    	
    	main_win = obj;
    	
        objectsList = new Vector<GraphicsItem>();
        setPreferredSize(new Dimension(200, 100));
        setBackground(Color.black);
        scale = 1.0;
        addMouseListener(this);
    }

     void doDrawing(Graphics g) {

        Graphics2D g2d = (Graphics2D) g;
        g2d.setColor(Color.BLACK);
        
        Dimension size = getSize();
        Insets insets = getInsets();
        int w = size.width - insets.left - insets.right;
        int h = size.height - insets.top - insets.bottom;
        System.out.println("Total width "+ size.width);
        System.out.println("Total height "+ size.height);
        System.out.println("Total insets(l,r,t,b)"+ insets.left +" " +insets.right +" "+insets.top+" "+ insets.bottom);
        System.out.println(" width "+w);
        System.out.println(" height "+h);
        Random r = new Random();
        //int x = Math.abs(r.nextInt()) % w;
        //int y = Math.abs(r.nextInt()) % h;
        g2d.drawLine(10, 10, 30, 20);

    }
     
     void drawRectangle(Graphics g) {

        Graphics2D g2d = (Graphics2D) g;
        g2d.setColor(Color.BLACK);
        g2d.drawRect(0,0,815, 815);
    }
     
    void drawEllipse(Graphics g) {

        Graphics2D g2d = (Graphics2D) g;
        g2d.setColor(Color.BLACK);
        

    }
       
    public void setScale(double s)  
    {  
        scale = s;  
        revalidate();      // update the scroll pane  
        repaint();  
    }  
   

    //Draw all the objects in the vector
    public void customDraw(Graphics g){
        Graphics2D g2d = (Graphics2D) g;
        AffineTransform oldAT = g2d.getTransform();
        
        Insets insets = getInsets();
	 int w = getWidth() - insets.left - insets.right;
	int h = getHeight() - insets.top - insets.bottom;
        oldAT.scale(scale, scale);  
        for(int i =0;i<objectsList.size();i++){
            try{
                GraphicsItem item = objectsList.elementAt(i);
                Shape itemShape = item.graphicsItem;
                
                itemShape = oldAT.createTransformedShape( itemShape );

                g2d.scale(1.0, -1.0);
                	
                g2d.translate(w/2, -h /2);
                if(main_win.undoRedoChangeLayers) {
                    g2d.translate(-457, -56);
                }
                g2d.setColor(item.color);
                g2d.draw(itemShape);
                if(item.brush){
                    g2d.fill(itemShape);
                }
                
            }catch(Exception e ){
                //e.printStackTrace();
            }finally {
		//restore
                
		g2d.setTransform(oldAT);
                
            }
        }
        if(main_win.undoRedoChangeLayers) {
            main_win.undoRedoChangeLayers = false;
        }
			    
        
    }
    
    public void drawArcTest(Graphics g){
        Graphics2D g2d = (Graphics2D) g;
            g2d.setColor(Color.gray);
            Arc2D.Double arc = new Arc2D.Double(0,0,100,100,90,135,Arc2D.OPEN ) ;
            g2d.draw(arc);
    }

    /*
     Maintain a list of objects. in paintcomponent, call one method called customDraw, will iterate over the list of objects  to be drawn
    * 
    */
    public void addObjectsToList(GraphicsItem item){
        objectsList.add(item);
        
    }
    public void clear(){
        objectsList.clear();
        
    }
    
    // Converts the points from 2D mouse click coordinates into Poincare coordinates
    public void convertPointsToPoincare(){
        poincare_x = ((2.0 * click_x)/(float)max_x)-1;
        // Add 0.035 to shift the poincare origin down
        poincare_y = 1-((2.0 * click_y)/(float)max_y);
    }
    
    public ArrayList<MotifPoint> getPoints(){
    	return poincare_xy;
    }
    
    public void clearPoints(){
    	poincare_xy.clear();
    }
    public boolean checkBoundingLimit(){
        double radius;
        radius = Math.sqrt(Math.pow(poincare_x, 2) + Math.pow(poincare_y, 2));
        System.out.println("Radius is: " + radius);
        return radius < 1;
    }
    

    @Override
    public void paintComponent(Graphics g) {    
    	super.paintComponent(g);
        System.out.println("paintComponent is called");
        //AffineTransformation scaleTransformation = AffineTransformation.getScaleInstance(xFactor, yFactor);
        //g.draw(theImage, scaleTransformation, null);
        //doDrawing(g);
        //drawRectangle(g);
        //drawArcTest(g);
        customDraw(g); //TODO Function to be called ultimately
    }

    public void mouseClicked(MouseEvent e)  {
    	
//    	try {
//    		main_win = new MainWindow();	
//    	}
//    	
//    	catch (InvalidIndexValueException ex)
//    	{
//    		System.out.println("caught invalid index value exception");
//    	}
   
        System.out.println("Mouse clicked. Coordinates are: " + e.getX()+ ", " + e.getY()); 
        click_x = e.getX();
        click_y = e.getY();
        convertPointsToPoincare();
        System.out.println("Poincare Coords:\n x: " + poincare_x + " y: " + poincare_y);
      
        try {
	        if(checkBoundingLimit())
	        {
	        	
	        //	System.out.println("the points size and object max" + main_win.points.size() + "    " + main_win.currentObjectMax);
	        //	if (main_win.objectPoints.size() == main_win.currentObjectMax)
	        //		JOptionPane.showMessageDialog(null, "you entered maximum number of points");
	        		
	       // 	else
	        	
	        	if(main_win.objectType == null)
	        		JOptionPane.showMessageDialog(null, "Please pay attention to the notes.");
	        	else
	        		main_win.addPoint(main_win.objectType);
//	        	currentObjectMaxPoints++;
//	        	MotifPoint m = new MotifPoint(poincare_x, poincare_y, 0, 0, 0);
//	            poincare_xy.add(m);
	        }
	        else{
	        	JOptionPane.showMessageDialog(null, "Please click inside the bounding circle");
	        }
//	        if(currentObjectMaxPoints == main_win.currentObjectMax){
//	          	
//	        }
        }
        
        catch (Exception exe)
    	{
        	System.out.println("Caught exception");
    	}
         
    }
    
    public void mousePressed(MouseEvent e){
    }
    
    public void mouseReleased(MouseEvent e){
    }
    
    public void mouseEntered(MouseEvent e){
        // System.out.println("Mouse entered. Coordinates are: " + e.getX()+ ", " + e.getY());
    }
    
    public void mouseExited(MouseEvent e){
        // System.out.println("Mouse exited. Coordinates are: " + e.getX()+ ", " + e.getY());
    }
    
}