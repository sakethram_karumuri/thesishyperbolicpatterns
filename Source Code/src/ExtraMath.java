/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author maneeshavejendla
 */
public class ExtraMath {
    
    
   ExtraMath(){
       
   }
   
 ////////////////////////////////////////////////////////////////////////////////
 // myCosh
 //////////////////////////////////////////////////////////////////////////////// 
    
 /*!
 * \brief a function to compute the hyperbolic cosine of b using p and q
 *
 * \param p The p value to use
 * \param q The q value to use
 *
 * \return The hyperbolic consine of b (cosh(b))using p and q as needed for this
 *         project
 */
static double myCosh(int p, int q)
{
  return (Math.cos(Math.PI / (double) q) / Math.sin(Math.PI/ (double) p));
} // myCosh

////////////////////////////////////////////////////////////////////////////////
// myCosh2
//////////////////////////////////////////////////////////////////////////////// 
    
/*!
 * \brief a function to compute the hyperbolic cosine of 2*b using p and q
 *
 * \param p The p value to use
 * \param q The q value to use
 *
 * \return The hyperbolic consine of 2*b (cosh(2*b))using p and q as needed for
 *         this project
 */
static double myCosh2(int p, int q)
{
  double coshValue = myCosh(p, q);
  return 2.0 * coshValue * coshValue - 1.0;
} // myCosh2
    
////////////////////////////////////////////////////////////////////////////////
// mySinh2
//////////////////////////////////////////////////////////////////////////////// 
 
/*!
 * \brief a function to compute the hyperbolic sine of 2*b using p and q
 *
 * \param p The p value to use
 * \param q The q value to use
 *
 * \return The hyperbolic sine of 2*b (sinh(2*b))using p and q as needed for
 *         this project
 */

//TODO The function should not take the negative values in is square root.
static double mySinh2(int p, int q)
{
  double cosh2Value = myCosh2(p, q);
  return ( Math.sqrt(cosh2Value * cosh2Value - 1.0 ) );
} // mySinh2


public  static void main(String args[]){
    System.out.println("The following methods tests the extra math class");
    int p = 3;
    int q = 5;
    ExtraMath m = new ExtraMath();
    double mycoshValue = m.myCosh(p,q);
    double mycosh2Value = m.myCosh2(p, q);
    double mysinh2Value = m.mySinh2(p, q);
    System.out.println("The values of the functions are :");
    System.out.println(mycoshValue);
    System.out.println(mycosh2Value);
    System.out.println(mysinh2Value);
    
    
}

}
