import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.DecompositionSolver;
import org.apache.commons.math3.linear.LUDecomposition;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.LineSegment;


public class CircleUtil {

	
	@SuppressWarnings("finally")
	public static VectorList findCenter(VectorList tempPoint, VectorList tempPoint2, VectorList tempPoint3) {
	 	Coordinate c11, c12, c21;
	 	VectorList new_center = null;
		try {
			c11 = new Coordinate(tempPoint.getElement(1), tempPoint.getElement(2));
			c12 = new Coordinate(tempPoint2.getElement(1), tempPoint2.getElement(2));
			c21 = new Coordinate(tempPoint3.getElement(1), tempPoint3.getElement(2));
		
			LineSegment ls = new LineSegment(c11, c12);
			LineSegment ls2 = new LineSegment(c12, c21);
		
			double slope_pb1 =0;
			double slope_pb2 =0;
			
			double slope1 = (c12.y - c11.y) / (c12.x - c11.x);
			slope_pb1 = -1 / slope1;
	
			double slope2 = (c21.y - c12.y) / (c21.x - c12.x);
			slope_pb2 = -1 / slope2;
		
			Coordinate midpb1 = ls.midPoint();
			Coordinate midpb2 = ls2.midPoint();

			double[] row1, row2;
			double c1, c2;
			if(Double.isInfinite(slope_pb1)) {
				c1 = midpb1.x;
				row1 = new double[] {1, 0};
			}
			else{
				c1 = midpb1.y - midpb1.x*slope_pb1;
				row1 =  new double[] { -1 * slope_pb1, 1 };
			}
		
			if(Double.isInfinite(slope_pb2)){
				c2 = midpb2.x;
				row2 = new double[] {1, 0};
			}
			else{
				c2 = midpb2.y - midpb2.x*slope_pb2;
				row2 =  new double[] { -1 * slope_pb2, 1 };
			}
			
			RealMatrix coefficients =
				    new Array2DRowRealMatrix(new double[][] { row1, row2},
				                       false);
			DecompositionSolver solver = new LUDecomposition(coefficients).getSolver();
				
			RealVector constants = new ArrayRealVector(new double[] { c1, c2 }, false);
			RealVector solution = solver.solve(constants);
			
			new_center= new VectorList(2);
			new_center.setElement(1, solution.getEntry(0));
			new_center.setElement(2, solution.getEntry(1));
			
//			System.out.println("New Center via LU is: " + solution);
		} catch (InvalidIndexValueException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			return new_center;
		}
	}

}
