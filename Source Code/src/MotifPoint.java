
import java.util.Vector;

/*!
 * \file motifpoint.java
 * \author Maneesha Vejendla <vejendlam@gmail.com>
 *
 * \brief MotifPoint - Structure to represent a point in a motif
 */

/**
 *
 * @author maneeshavejendla
 */

public class MotifPoint {
    double x;
    double y;
    double w;
    int motifColor;
    int pointTyp;  
    
    MotifPoint(){
        this.x = 0.0;
        this.y = 0.0;
        this.w = 0.0;
        this.motifColor = 0;
        this.pointTyp = 0;
    }
    
     MotifPoint(MotifPoint m ){
        this.x = m.x;
        this.y = m.y;
        this.w = m.w;
        this.motifColor = m.motifColor;
        this.pointTyp = m.pointTyp;
    }
    
    
    MotifPoint(double x, double y, double w, int motifColor, int pointType){
        this.x = x;
        this.y = y;
        this.w = w;
        this.motifColor = motifColor;
        this.pointTyp = pointType;
    }
    
    void printElements(){
        
        System.out.println("The elements of motifpoint are: ");
        System.out.println(this.x);
        System.out.println(this.y);
        System.out.println(this.w);
        System.out.println(this.motifColor);
        System.out.println(this.pointTyp);        
    }
     public static void main(String args[]) {
        MotifPoint m1 = new MotifPoint(1.0,1.0,1.0,0,1);
        MotifPoint m2 = new MotifPoint(2.0,2.0,2.0,1,2);
        MotifPoint m3 = new MotifPoint(3.0,3.0,3.0,2,3);
        Vector<MotifPoint> v = new Vector<MotifPoint>();
        v.add(m1);
        v.add(m2);
        v.add(m3);
        System.out.println("the size of the vector od motif points is");
        System.out.println(v.size());
        m1.printElements();
        
    }
}
